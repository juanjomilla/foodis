<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartDetails extends Model
{
    protected $table = 'cart_details';

    protected $fillable = [
        'id', 'id_cart', 'id_menu', 'qty'
    ];
}
