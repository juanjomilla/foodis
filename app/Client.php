<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';

    protected $fillable = [
        'id', 'id_user', 'client_name',
        'client_last_name', 'street', 'street_number',
        'city', 'province', 'lat', 'long'
    ];
}
