<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commerce extends Model
{
    protected $table = 'commerce';

    protected $fillable = [
        'id', 'id_user', 'commerce_name',
        'cuit', 'street', 'street_number',
        'city', 'province', 'lat', 'long',
        'delivery_radio', 'enabled'
    ];
}
