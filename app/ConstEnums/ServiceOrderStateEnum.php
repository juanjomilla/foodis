<?php

namespace App\ConstEnums;

class ServiceOrderStateEnum
{
    const OutstandingPayment = 1;
    const Paid = 2;
    const DeliveryAssigned = 3;
    const InTransit = 4;
    const Shipped = 5;
}