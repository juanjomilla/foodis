<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 20/10/2018
 * Time: 15:35
 */

namespace App\ConstEnums;

class UserTypeEnum
{
    const Commerce = 1;
    const Client = 2;
    const Delivery = 3;
    const Admin = 4;
    const CommerceUser = 5;
}