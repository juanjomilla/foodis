<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'delivery';

    protected $fillable = [
        'id', 'id_user', 'delivery_name',
        'delivery_last_name', 'street', 'street_number',
        'city', 'province', 'enabled', 'picked_up_late'
    ];
}
