<?php

namespace App\Http\Controllers\Admin;

use App\Delivery;
use App\ServiceOrderClearing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\JSON\JSONController;
use App\Commerce;

class AdminController extends Controller
{
    public function makeClearing(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {
                $clearedServiceOrder = new ServiceOrderClearing();
                $clearedServiceOrder = $clearedServiceOrder->where([
                    'cleared' => 0
                ])->get();

                foreach ($clearedServiceOrder as $serviceOrder){
                    $serviceOrder->cleared = 1;
                    $serviceOrder->cleared_date = date('Y-m-d H:i:s');
                    $serviceOrder->save();
                }

                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'data_response' => 'Se han liquidado correctamente las ordenes de servicio',
                        'details' => []
                    ]
                ];

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getCommercesDisabled(Request $req) {
        return $this->getCommerces($req, 0);
    }

    public function getCommercesEnabled(Request $req) {
        return $this->getCommerces($req, 1);
    }

    public function getDeliveriesDisabled(Request $req) {
        return $this->getDeliveries($req, 0);
    }

    public function getDeliveriesEnabled(Request $req) {
        return $this->getDeliveries($req, 1);
    }

    public function disableDelivery(Request $req) {
        return $this->enableOrDisableDelivery($req, 0);
    }

    public function enableDelivery(Request $req) {
        return $this->enableOrDisableDelivery($req, 1);
    }

    public function enableCommerce(Request $req) {
        return $this->enableOrDisableCommerce($req, 1);
    }

    public function disableCommerce(Request $req) {
            return $this->enableOrDisableCommerce($req, 0);
    }

    public function getCompletedServiceOrderWithoutClearing(Request $req) {
        return $this->getCompletedServiceOrderWithOrWithoutClearing($req, 0);
    }

    public function getCompletedServiceOrderWithClearing(Request $req) {
        return $this->getCompletedServiceOrderWithOrWithoutClearing($req, 1);
    }

    private function getCompletedServiceOrderWithOrWithoutClearing(Request $req, $cleared) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {
                $clearedServiceOrder = new ServiceOrderClearing();
                $clearedServiceOrder = $clearedServiceOrder->where([
                    'cleared' => $cleared
                ])->get();

                $dataToReturn = $clearedServiceOrder->map(function($serviceOrder) {
                    return [
                        'id_service_order' => $serviceOrder->id_service_order,
                        'pay_to_commerce' => $serviceOrder->pay_to_commerce,
                        'pay_to_delivery' => $serviceOrder->pay_to_delivery,
                        'cleared_date' => $serviceOrder->cleared_date,
                        'picked_up_late' => $serviceOrder->picked_up_late,
                        'delivered_late' => $serviceOrder->delivered_late
                    ];
                });

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function enableOrDisableDelivery(Request $req, $enable) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {

                if($enable == 1) {
                    $messageSuccessfully = 'Delivery habilitado exitosamente';
                } else {
                    $messageSuccessfully = 'Delivery deshabilitado exitosamente';
                }

                $idDelivery = $json->getParameters()->id_delivery;
                $delivery = new Delivery();
                $delivery = $delivery->find($idDelivery);

                $delivery->enabled = $enable;
                $delivery->save();

                $dataToReturn = [
                    'succes' => [
                        'code' => 200,
                        'message' => $messageSuccessfully,
                        'details' => []
                    ]
                ];

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function enableOrDisableCommerce(Request $req, $enable) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {

                if($enable == 1) {
                    $messageSuccessfully = 'Comercio habilitado exitosamente';
                } else {
                    $messageSuccessfully = 'Comercio deshabilitado exitosamente';
                }

                $idCommerce = $json->getParameters()->id_commerce;
                $commerce = new Commerce();
                $commerce = $commerce->find($idCommerce);

                $commerce->enabled = $enable;
                $commerce->save();
                $dataToReturn = [
                    'succes' => [
                        'code' => 200,
                        'message' => $messageSuccessfully,
                        'details' => []
                    ]
                ];

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function getCommerces(Request $req, $enabled) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {

                $commerces = new Commerce();
                $commerces = $commerces->where([
                    'enabled' => $enabled
                ])->get();

                $dataToReturn = $commerces->map(function ($commerce) {
                    return [
                        'id_commerce' => $commerce->id,
                        'name' => $commerce->commerce_name,
                        'cuit' => $commerce->cuit,
                        'direction' => $commerce->street . " " . $commerce->street_number,
                        'city' => $commerce->city,
                        'delivery_radio' => $commerce->delivery_radio
                    ];
                });


            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function getDeliveries(Request $req, $enabled) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // the json is valid...

            try {

                $deliveries = new Delivery();
                $deliveries = $deliveries->where([
                    'enabled' => $enabled
                ])->get();

                $dataToReturn = $deliveries->map(function ($delivery) {
                    return [
                        'id_delivery' => $delivery->id,
                        'name' => $delivery->delivery_name,
                        'last_name' => $delivery->delivery_last_name,
                        'cuit' => $delivery->dni,
                        'direction' => $delivery->street . " " . $delivery->street_number,
                        'city' => $delivery->city,
                    ];
                });

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }
}
