<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 24/11/2018
 * Time: 23:06
 */

namespace App\Http\Controllers\CUIT;

use App\Http\Controllers\Controller;


class CUITController extends Controller
{
    public function validateCUIT($cuit) {
        $digits = array();
        if (strlen($cuit) != 13) {
            throw new \Exception('CUIT invalido');
        }
        for ($i = 0; $i < strlen($cuit); $i++) {
            if ($i == 2 or $i == 11) {
                if ($cuit[$i] != '-') {
                    throw new \Exception('CUIT invalido');
                }
            } else {
                if (!ctype_digit($cuit[$i])) {
                    throw new \Exception('CUIT invalido');
                }
                if ($i < 12) {
                    $digits[] = $cuit[$i];
                }
            }
        }
        $acum = 0;
        foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
            $acum += $digits[$i] * $multiplicador;
        }
        $cmp = 11 - ($acum % 11);
        if ($cmp == 11) $cmp = 0;
        if ($cmp == 10) $cmp = 9;
        if ($cuit[12] != $cmp) {
            throw new \Exception('CUIT invalido');
        }
    }
}