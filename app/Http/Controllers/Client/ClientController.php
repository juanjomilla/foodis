<?php

namespace App\Http\Controllers\Client;

use App\Cart;
use App\CartDetails;
use App\Commerce;
use App\ConstEnums\ServiceOrderStateEnum;
use App\Http\Controllers\Localization\PointMap;
use App\Menu;
use App\ServiceOrder;
use App\ServiceOrderClearing;
use App\ServiceOrderDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\JSON\JSONController;
use App\Client;


class ClientController extends Controller
{
    public function finishRegister(Request $req)
    {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // The json is valid...
            \DB::beginTransaction();

            try {
                $param = $json->getParameters();
                $client = new Client();

                $client->id_user = $json->getAuthParameter('id_user');
                $client->client_name = $param->client_name;
                $client->client_last_name = $param->client_last_name;
                $client->dni = $param->dni;
                $client->street = $param->street;
                $client->street_number = $param->street_number;
                $client->city = $param->city;
                $client->province = $param->province;
                $client->lat = $param->lat;
                $client->long = $param->long;

                $req->user()->finishRegister();

                $client->save();

                \DB::commit();

                $result = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Cliente registrado correctamente',
                        'details' => []
                    ]
                ];
            } catch (\Exception $e) {
                $result = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
                \DB::rollback();
            }
        } else {
            // error
            $result = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($result);
    }

    public function getAllCommercesFromUser(Request $req)
    {
        $input = $req->input('data_request');
        $json = new JSONController();
        $dataToReturn = [];

        if ($json->create($input)) {
            // the json is valid...

            try {
                $idUser = $json->getAuthParameter('id_user');
                $idClient = $this->getIdClientFromIdUser($idUser);

                $client = new Client();
                $client = $client->find($idClient);

                $clientUbication = new PointMap($client->lat, $client->long);

                $commerces = new Commerce();
                $commerces = $commerces->get();
                foreach ($commerces as $commerce) {
                    $distance = $clientUbication->distanceToPoint(new PointMap($commerce->lat, $commerce->long));

                    $infoCommerce = [
                        'commerce_name' => $commerce->commerce_name,
                        'id_commerce' => $commerce->id,
                        'distance' => $distance
                    ];
                    if ($distance <= $commerce->delivery_radio) {
                        array_push($dataToReturn, $infoCommerce);
                    }
                }
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getIdCommerceFromUserCart(Request $req)
    {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $cart = new Cart();
                $cart = $cart->where(['id_client' => $idUser])->first();
                if (is_null($cart)) {
                    $dataToReturn = null;
                } else {
                    $dataToReturn = $cart->id_commerce;
                }
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getCartFromUser(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $idCart = $this->getIdCartFromUser($idUser);

                $dataToReturn = [];
                if (is_null($idCart)) {
                    $dataToReturn = null;
                } else {
                    $cartDetails = new CartDetails();
                    $cartDetails = $cartDetails->where(['id_cart' => $idCart])->get();
                    foreach ($cartDetails as $detail) {
                        $menuData = $this->getMenuData($detail->id_menu);
                        $menu = [
                            'id_menu' => $detail->id_menu,
                            'menu_name' => $menuData['menuName'],
                            'description' => $menuData['description'],
                            'price' => $menuData['price'],
                            'qty' => $detail->qty,
                            'url_img' => $menuData['urlImg']
                        ];
                        array_push($dataToReturn, $menu);

                    }
                }
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function emptyCart(Request $req)
    {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $idCart = $this->getIdCartFromUser($idUser);

                $cartDetails = new CartDetails();
                $cartDetails->where(['id_cart' => $idCart])->delete();

                $cart = new Cart();
                $cart->where(['id' => $idCart])->delete();

                $dataToReturn = [
                    'succes' => [
                        'code' => 200,
                        'message' => 'Carrito vaciado correctamente',
                        'details' => []
                    ]
                ];
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function addToCart(Request $req)
    {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $param = $json->getParameters();
                $idUser = $json->getAuthParameter('id_user');
                $idCart = $this->getIdCartFromUser($idUser);

                $idCommerce = $param->id_commerce;
                $idMenu = $param->id_menu;
                $qty = $param->qty;

                if (is_null($idCart)) {
                    // no tengo carrito, lo creo...
                    $idCart = $this->createCart($idUser, $idCommerce);
                }

                if ($this->insertMenuInCart($idMenu, $idCart, $qty)) {
                    $dataToReturn = [
                        'succes' => [
                            'code' => 200,
                            'message' => 'Menu insertado correctamente',
                            'details' => []
                        ]
                    ];
                } else {
                    $dataToReturn = [
                        'error' => [
                            'code' => '0235',
                            'message' => 'No se pudo cargar el menu en el comercio',
                            'details' => []
                        ]
                    ];
                }

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getMenuFromCommerce(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $param = $json->getParameters();

                $idCommerce = $param->id_commerce;

                $menus = new Menu();
                $menus = $menus->where(['id_commerce' => $idCommerce])->get();

                $dataToReturn = $menus->map(function($menu) {
                    return [
                        'menu_id' => $menu->id,
                        'menu_name' => $menu->menu_name,
                        'description' => $menu->description,
                        'price' => $menu->price,
                        'url_img' => $menu->url_img
                    ];
                });
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function confirmCart(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                \DB::beginTransaction();

                $totalPrice = $json->getParameters()->total_price;
                $idUser = $json->getAuthParameter('id_user');
                $idClient = $this->getIdClientFromIdUser($idUser);
                $idCart = $this->getIdCartFromUser($idUser);

                $cart = new Cart();
                $cartDetails = new CartDetails();

                $cart = $cart->find($idCart);
                $cartDetails = $cartDetails->where(['id_cart' => $idCart])->get();

                $serviceOrder = new ServiceOrder();
                $clearingServiceOrder = new ServiceOrderClearing();


                $serviceOrder->id_client = $idClient;
                $serviceOrder->id_commerce = $cart->id_commerce;
                $serviceOrder->total_price = $totalPrice;
                $serviceOrder->save();

                $idServiceOrder = $serviceOrder->id;

                $clearingServiceOrder->id_service_order = $idServiceOrder;
                $clearingServiceOrder->pay_to_commerce = $totalPrice * 0.92;
                $clearingServiceOrder->save();

                foreach ($cartDetails as $detail) {
                    $serviceOrderDetail = new ServiceOrderDetails();

                    $serviceOrderDetail->id_service_order = $idServiceOrder;
                    $serviceOrderDetail->id_menu = $detail->id_menu;
                    $serviceOrderDetail->qty = $detail->qty;

                    $serviceOrderDetail->save();
                }

                $cart = new Cart();
                $cartDetails = new CartDetails();
                $cartDetails->where(['id_cart' => $idCart])->delete();
                $cart->where(['id' => $idCart])->delete();

                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Orden de servicio generada con éxito',
                        'details' => []
                    ]
                ];



                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function payServiceOrder(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                \DB::beginTransaction();

                $idServiceOrder = $json->getParameters()->id_service_order;

                $serviceOrder = new ServiceOrder();
                $serviceOrder = $serviceOrder->find($idServiceOrder);
                $serviceOrder->id_state = ServiceOrderStateEnum::Paid;
                $serviceOrder->save();

                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Se modificó el estado de la orden de serivicio a "Pagado"',
                        'details' => []
                    ]
                ];

                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getServiceOrder(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idUser = $json->getAuthParameter('id_user');
                $idState = $json->getParameters()->id_state;

                $serviceOrders = new ServiceOrder();
                $serviceOrders = $serviceOrders->where(['id_client' => $this->getIdClientFromIdUser($idUser), 'id_state' => $idState])->get();

                $dataToReturn = $serviceOrders->map(function($order) {
                 return [
                     'id' => $order->id,
                     'id_state' => $order->id_state,
                     'id_delivery' => $order->id_delivery,
                     'price' => $order->total_price,
                     'date' => $order->created_at

                 ];
             });

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function updateItemQty(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $param = $json->getParameters();
                $idUser = $json->getAuthParameter('id_user');
                $idCart = $this->getIdCartFromUser($idUser);

                $idMenu = $param->id_menu;
                $newQty = $param->qty;
                $cartDetails = new CartDetails();
                $cartDetails = $cartDetails->where(['id_cart' => $idCart, 'id_menu' => $idMenu])->first();
                if ($newQty < 1) {
                    $cartDetails->delete();
                } else {
                    $cartDetails->qty = $newQty;
                    $cartDetails->save();
                }

                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Cantidad actualizada correctamente',
                        'details' => []
                    ]
                ];
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getGeoPosition(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idClient = $json->getParameters()->id_client;
                $client = new Client();
                $client = $client->find($idClient);
                $dataToReturn =  [
                    'lat' => $client->lat,
                    'long' => $client->long
                ];

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function createCart($idUser, $idCommerce)
    {
        try {
            $cart = new Cart();
            $cart->id_client = $idUser;
            $cart->id_commerce = $idCommerce;
            $cart->save();

            return $cart->id;
        } catch (\Exception $e) {
            throw new \Exception('No se pudo crear el carrito. ' . $e->getMessage());
        }
    }

    private function getMenuData($idMenu)
    {
        $menu = new Menu();
        $menu = $menu->find($idMenu);

        return [
            'menuName' => $menu->menu_name,
            'description' => $menu->description,
            'price' => $menu->price,
            'urlImg' => $menu->url_img
        ];
    }

    private function getIdCartFromUser($idUser)
    {
        $cart = new Cart();
        $cart = $cart->where(['id_client' => $idUser])->first();

        if (is_null($cart)) {
            return null;
        } else {
            return $cart->id;
        }
    }

    private function getIdClientFromIdUser($idUser)
    {
        $client = new Client();
        $client = $client->where(['id_user' => $idUser])->first();

        try {
            return $client->id;
        } catch (\Exception $e) {
            throw new \Exception("Usuario Cliente no encontrado. " . $e->getMessage());
        }
    }

    private function insertMenuInCart($idMenu, $idCart, $qty)
    {
        $cartDetail = new CartDetails();
        $cartDetail = $cartDetail->where(['id_cart' => $idCart, 'id_menu' => $idMenu])->first();

        if (is_null($cartDetail)) {
            $cartDetail = new CartDetails();
            $cartDetail->id_menu = $idMenu;
            $cartDetail->id_cart = $idCart;
            $cartDetail->qty = $qty;
        } else {
            $cartDetail->qty += $qty;
        }

        return $cartDetail->save();
    }
}