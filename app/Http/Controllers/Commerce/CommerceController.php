<?php

namespace App\Http\Controllers\Commerce;

use App\ConstEnums\UserTypeEnum;
use App\ServiceOrder;
use App\Http\Controllers\CUIT\CUITController;
use App\Menu;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\JSON\JSONController;
use App\Commerce;
use Illuminate\Support\Facades\Storage;

class CommerceController extends Controller
{
    public function finishRegister(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if($json->create($input)) {
            // The json is valid...
            \DB::beginTransaction();

            try {
                $param = $json->getParameters();
                $commerce = new Commerce();
                $cuit = new CUITController();
                $cuit->validateCUIT($param->cuit);

                $commerce->id_user = $json->getAuthParameter('id_user');
                $commerce->commerce_name = $param->commerce_name;
                $commerce->cuit = $param->cuit;
                $commerce->street = $param->street;
                $commerce->street_number = $param->street_number;
                $commerce->city = $param->city;
                $commerce->province = $param->province;
                $commerce->lat = $param->lat;
                $commerce->long = $param->long;
                $commerce->delivery_radio = $param->delivery_radio;

                $req->user()->finishRegister();

                $commerce->save();

                \DB::commit();

                $result = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Comercio registrado correctamente',
                        'details' => []
                    ]
                ];
            } catch (\Exception $e) {
                $result = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
                \DB::rollback();
            }
        } else {
            // error
            $result = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($result);
    }

    public function getAllMenuFromCommerce(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if($json->create($input)) {
            // the json is valid...

            try {
                $idUser = $json->getAuthParameter('id_user');
                $idCommerce = $this->getIdCommerceFromIdUser($idUser);
                $menu = new Menu();
                $menu = $menu->where(['id_commerce' => $idCommerce])->get();

                $dataToReturn = $menu->map(function ($menu) {
                    return [
                        'menu_id' => $menu->id,
                        'menu_name' => $menu->menu_name,
                        'price' => $menu->price,
                        'description' => $menu->description,
                        'url_img' => $menu->url_img
                    ];
                });
            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }

        return json_encode($dataToReturn);
    }

    public function saveMenu(Request $req) {
        $jsonRequestInput = $req->input('data_request');
        $jsonRequest = new JSONController();

        if ($jsonRequest->create($jsonRequestInput)) {
            $parameters = $jsonRequest->getParameters();
            $idUser = $jsonRequest->getAuthParameter('id_user');
            $idCommerce = $this->getIdCommerceFromIdUser($idUser);

            \DB::beginTransaction();
            try {
                $base64img = $parameters->base64img;
                $base64MimeType = strstr($base64img, ';base64,', true);
                $base64MimeType = str_replace('data:', '', $base64MimeType);
                $base64img = str_replace('data:' . $base64MimeType . ';base64,', '', $base64img);
                $base64img = str_replace(' ', '+', $base64img);

                $fileExtension = $this->getFileExtension($base64MimeType);
                $imageName = str_random(20);
                $imageName = md5($imageName . $idUser) . '.' . $fileExtension;

                $newMenu = new Menu();

                $newMenu->id_commerce = $idCommerce;
                $newMenu->menu_name = $parameters->menu_name;
                $newMenu->description = $parameters->description;
                $newMenu->url_img = '';
                $newMenu->price = $parameters->price;
                $newMenu->save();

                $newMenu->url_img = $imageName;
                $newMenu->save();
                Storage::disk('menuImg')->put($imageName, base64_decode($base64img));
                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Se ha creado con éxito el menú.',
                        'details' => []
                    ]
                ];

                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => 500,
                    'message' => 'No se pudo guardar el menú.',
                    'details' => $jsonRequest->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getServiceOrder(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idUser = $json->getAuthParameter('id_user');
                $idState = $json->getParameters()->id_state;

                $serviceOrders = new ServiceOrder();
                $serviceOrders = $serviceOrders->where(['id_commerce' => $this->getIdCommerceFromIdUser($idUser), 'id_state' => $idState])->get();

                $dataToReturn = $serviceOrders->map(function($order) {
                    return [
                        'id' => $order->id,
                        'id_state' => $order->id_state,
                        'id_delivery' => $order->id_delivery,
                        'price' => $order->total_price,
                        'date' => $order->created_at
                    ];
                });

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getCommerceUsers(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idUser = $json->getAuthParameter('id_user');
                $idState = $json->getParameters()->id_state;

                $commerceUsers = new User();
                $commerceUsers = $commerceUsers->where([
                    'id_type' => UserTypeEnum::CommerceUser
                ])->get();

                $serviceOrders = new ServiceOrder();
                $serviceOrders = $serviceOrders->where([
                    'id_commerce' => $this->getIdCommerceFromIdUser($idUser),
                    'id_state' => $idState
                ])->get();

                $dataToReturn = $serviceOrders->map(function($order) {
                    return [
                        'id' => $order->id,
                        'id_state' => $order->id_state,
                        'state' => $order->getState()->first()->text,
                        'id_delivery' => $order->id_delivery
                    ];
                });

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getGeoPosition(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idCommerce = $json->getParameters()->id_commerce;
                $commerce = new Commerce();
                $commerce = $commerce->find($idCommerce);

                $dataToReturn =  [
                        'lat' => $commerce->lat,
                        'long' => $commerce->long
                    ];

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function getIdCommerceFromIdUser($idUser) {
        $commerce = new Commerce();
        $commerce = $commerce->where(['id_user' => $idUser])->first();

        if (is_null($commerce)) {
            return null;
        } else {
            return $commerce->id;
        }
    }

    private function getFileExtension($mimeType) {
        $fileExtension = '';

        switch ($mimeType) {
            case 'image/jpeg':
                $fileExtension = '.jpg';
                break;
            case 'image/png':
                $fileExtension = '.png';
                break;
        }

        return $fileExtension;
    }

}
