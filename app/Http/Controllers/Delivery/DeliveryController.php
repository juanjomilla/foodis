<?php

namespace App\Http\Controllers\Delivery;

use App\Commerce;
use App\ServiceOrder;
use App\ServiceOrderClearing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\JSON\JSONController;
use App\Delivery;
use Illuminate\Support\Facades\Mail;
use App\Mail\SuccessfullyFinishedRegister;
use App\ConstEnums\ServiceOrderStateEnum;

class DeliveryController extends Controller
{
    public function finishRegister(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if($json->create($input)) {
            // The json is valid...
            \DB::beginTransaction();

            try {
                $param = $json->getParameters();
                $delivery = new Delivery();

                $delivery->id_user = $json->getAuthParameter('id_user');
                $delivery->delivery_name = $param->delivery_name;
                $delivery->delivery_last_name = $param->delivery_last_name;
                $delivery->dni = $param->dni;
                $delivery->street = $param->street;
                $delivery->street_number = $param->street_number;
                $delivery->city = $param->city;
                $delivery->province = $param->province;
                $delivery->vehicle = $param->vehicle;

                $req->user()->finishRegister();

                $delivery->save();
/*
                $emailDelivery = new \stdClass();
                $emailDelivery->name = $param->delivery_name;
                $emailDelivery->last_name = $param->delivery_last_name;
                $emailDelivery->user_type = 'Delivery';
                $emailDelivery->sender = 'El equipo de foodis';
                $emailDelivery->receiver = $param->delivery_name . $param->delivery_last_name;

                Mail::to($req->user()->email)->send(new SuccessfullyFinishedRegister($emailDelivery));
*/
                \DB::commit();

                $result = [
                    'success' => [
                        'code' => 200,
                        'message' => 'Delivery registrado correctamente',
                        'details' => []
                    ]
                ];
            } catch (\Exception $e) {
                $result = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
                \DB::rollback();
            }
        } else {
            // error
            $result = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($result);
    }

    public function getServiceOrder(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idState = $json->getParameters()->id_state;
                $serviceOrders = new ServiceOrder();
                $serviceOrders = $serviceOrders->where('id_delivery', '=', null)
                    ->where('id_state', '=', $idState)->get();



                $dataToReturn = $serviceOrders->map(function($order) {
                    return [
                        'id' => $order->id,
                        'id_state' => $order->id_state,
                        'id_delivery' => $order->id_delivery,
                        'price' => $order->total_price,
                        'date' => $order->created_at,
                        'id_client' => $order->id_client,
                        'id_commerce' => $order->id_commerce
                    ];
                });

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function getServiceOrderWithStateThree(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {

                $idUser = $json->getAuthParameter('id_user');
                $delivery = new Delivery();
                $delivery = $delivery->where('id_user', '=', $idUser)->first();
                $idDelivery = $delivery->id;
                $idState = $json->getParameters()->id_state;
                $serviceOrders = new ServiceOrder();
                $serviceOrders = $serviceOrders->where('id_delivery', '=', $idDelivery)
                    ->where('id_state', '=', $idState)->get();
                $dataToReturn = $serviceOrders->map(function($order) {
                    return [
                        'id' => $order->id,
                        'id_state' => $order->id_state,
                        'id_delivery' => $order->id_delivery,
                        'price' => $order->total_price,
                        'date' => $order->created_at,
                        'id_commerce' => $order->id_commerce,
                        'id_client' => $order->id_client
                    ];
                });

            }catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function markAsShipped(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $idServiceOrder = $json->getParameters()->id_service_order;

                $idDelivery = $this->getIdDeliveryFromIdUser($idUser);

                $serviceOrder = new ServiceOrder();
                $delivery = new Delivery();

                $serviceOrder = $serviceOrder->where([
                    'id_delivery' => $idDelivery,
                    'id' => $idServiceOrder
                ])->first();

                $delivery = $delivery->find($idDelivery);

                if(is_null($serviceOrder)) {
                    $dataToReturn = [
                            'error' => [
                                'code' => 024,
                                'message' => 'La orden de servicio no está asociada al delivery',
                                'details' => []
                        ]
                    ];
                } else {
                    $serviceOrder->id_state = ServiceOrderStateEnum::Shipped;

                    $pickedUpTime = new Carbon($serviceOrder->picked_up_at);
                    $shippedTime = new Carbon();

                    $diffInMinutes = $shippedTime->diffInMinutes($pickedUpTime);

                    $commerce = new Commerce();
                    $clearingServiceOrder = new ServiceOrderClearing();

                    $commerce = $commerce->find($serviceOrder->id_commerce);
                    $clearingServiceOrder = $clearingServiceOrder->where([
                        'id_service_order' => $serviceOrder->id
                    ])->first();

                    $deliverLate = $diffInMinutes > ($commerce->minutes_to_deliver) * 1.15;

                    if ($deliverLate && $delivery->picked_up_late) {
                        // the delivery is late...
                        $clearingServiceOrder->pay_to_delivery = $serviceOrder->total_price * 0.2;
                        $clearingServiceOrder->delivered_late = 1;
                        $clearingServiceOrder->picked_up_late = 1;
                    } else if ($deliverLate && !$delivery->picked_up_late) {
                        $clearingServiceOrder->pay_to_delivery = $serviceOrder->total_price * 0.25;
                        $clearingServiceOrder->delivered_late = 1;
                    } else if (!$deliverLate && $delivery->picked_up_late) {
                        $clearingServiceOrder->pay_to_delivery = $serviceOrder->total_price * 0.25;
                        $clearingServiceOrder->picked_up_late = 1;
                    } else {
                        $clearingServiceOrder->pay_to_delivery = $serviceOrder->total_price * 0.3;
                    }

                    $serviceOrder->shipped_at = date('Y-m-d H:i:s');
                    $delivery->picked_up_late = 0;

                    $delivery->save();
                    $serviceOrder->save();
                    $clearingServiceOrder->save();

                    $dataToReturn = [
                        'success' => [
                            'code' => 200,
                            'message' => 'La orden de servicio fue marcada como entregada',
                            'details' => []
                        ]
                    ];
                }

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function markAsPickedUp(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $idServiceOrder = $json->getParameters()->id_service_order;
                $idDelivery = $this->getIdDeliveryFromIdUser($idUser);

                $serviceOrder = new ServiceOrder();

                $serviceOrder = $serviceOrder->where([
                    'id_delivery' => $idDelivery,
                    'id' => $idServiceOrder
                ])->first();

                if(is_null($serviceOrder)) {
                    $dataToReturn = [
                        'error' => [
                            'code' => 024,
                            'message' => 'La orden de servicio no está asociada al delivery',
                            'details' => []
                        ]
                    ];
                } else {
                    $serviceOrder->id_state = ServiceOrderStateEnum::InTransit;
                    $serviceOrder->picked_up_at = date('Y-m-d H:i:s');
                    $serviceOrder->save();

                    $dataToReturn = [
                        'success' => [
                            'code' => 200,
                            'message' => 'La orden de servicio fue marcada como entregada',
                            'details' => []
                        ]
                    ];
                }

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function assignOrder(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                \DB::beginTransaction();
                $idUser = $json->getAuthParameter('id_user');
                $idServiceOrder = $json->getParameters()->id_service_order;
                $delivery = new Delivery();
                $delivery = $delivery->where('id_user', '=', $idUser)->first();
                $serviceOrder = new ServiceOrder();
                $serviceOrder = $serviceOrder->where([
                    'id_delivery' => null,
                    'id' => $idServiceOrder,
                    'id_state' => 2
                ])->first();

                if(is_null($serviceOrder)) {
                    $dataToReturn = [
                        'error' => [
                            'code' => 024,
                            'message' => 'No se puede asignar la orden de servicio. Esto puede deberse a una o más razones.',
                            'details' => [
                                'reason' => [
                                    0 => 'La orden de servicio no existe',
                                    1 => 'La orden de servicio tiene asignado un delivery',
                                    2 => 'La orden de servicio está en un estado no permitido para esta operación'
                                ]
                            ]
                        ]
                    ];
                } else {
                    $serviceOrder->id_state = ServiceOrderStateEnum::DeliveryAssigned;
                    $serviceOrder->id_delivery = $delivery->id;

                    $delivery->save();
                    $serviceOrder->save();

                    $dataToReturn = [
                        'success' => [
                            'code' => 200,
                            'message' => 'La orden de servicio fue asignada exitosamente',
                            'details' => []
                        ]
                    ];
                }
                \DB::commit();

            } catch (\Exception $e) {
                \DB::rollback();
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    public function penalizeDelivery(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();

        if ($json->create($input)) {
            // valid json...
            try {
                $idUser = $json->getAuthParameter('id_user');
                $delivery = new Delivery();
                $delivery = $delivery->where('id_user', '=', $idUser)->first();
                $delivery->picked_up_late = 1;
                $delivery->save();

                $dataToReturn = [
                    'success' => [
                        'code' => 200,
                        'message' => 'El delivery ha sido penalizado',
                        'details' => []
                    ]
                ];

            } catch (\Exception $e) {
                $dataToReturn = [
                    'error' => [
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'details' => $e->getTrace()
                    ]
                ];
            }
        } else {
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }

    private function getIdDeliveryFromIdUser($idUser) {
        $delivery = new Delivery();
        $delivery = $delivery->where(['id_user' => $idUser])->first();

        if (is_null($delivery)) {
            return null;
        } else {
            return $delivery->id;
        }
    }

}
