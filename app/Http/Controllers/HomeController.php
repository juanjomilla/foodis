<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConstEnums\UserTypeEnum;
use App\Http\Controllers\JSON\JSONController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getHeaderLinks(Request $req) {
        $input = $req->input('data_request');
        $json = new JSONController();
        $dataToReturn = [];

        if ($json->createWithoutAuthParameters($input)) {

            $idUserType = $json->getParameters()->id_user_type;
            
            switch ($idUserType) {
                case UserTypeEnum::Commerce:
                $dataToReturn = array(
                    'menuLink' => array(
                        'text' => 'Menú',
                        'link' => url('/commerce/myMenu')
                    ),
                    'addMenuLink' => array(
                        'text' => 'Agregar menú',
                        'link' => url('/commerce/addMenu')
                    ),
                    'myServiceOrdersLink' => array(
                        'text' => 'Pedidos',
                        'link' => url('/commerce/serviceOrder')
                    )
                );
                break;
                case UserTypeEnum::Client:
                $dataToReturn = array(
                    'menuLink' => array(
                        'text' => 'Comercios',
                        'link' => url('/client/commerces')
                    ),
                    'cartLink' => array(
                        'text' => 'Carrito',
                        'link' => url('/client/cart')
                    ),
                    'myServiceOrdersLink' => array(
                        'text' => 'Ver mis pedidos',
                        'link' => url('/client/serviceOrder')
                    )
                );
                break;
                case UserTypeEnum::Admin:
                $dataToReturn = array(
                    'acceptCommercesLink' => array(
                        'text' => 'Comercios',
                        'link' => url('/admin/commercesList')
                    ),
                    'acceptDeliveriesLink' => array(
                        'text' => 'Deliveries',
                        'link' => url('/admin/deliveriesList')
                    ),
                    'settlementLink' => array(
                        'text' => 'Liquidaciones',
                        'link' => url('/admin/settlement')
                    )
                );
                break;
                case UserTypeEnum::Delivery:
                $dataToReturn = array(
                    'myServiceOrdersLink' => array(
                        'text' => 'Ver mis pedidos',
                        'link' => url('/delivery/serviceOrder')
                    )
                );
                break;
            } 
        } else {
            // error
            $dataToReturn = [
                'error' => [
                    'code' => '025',
                    'message' => 'JSON invalido',
                    'details' => $json->getError()
                ]
            ];
        }
        return json_encode($dataToReturn);
    }
}
