<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 18/11/2018
 * Time: 17:58
 */

namespace App\Http\Controllers\JSON;

use App\Http\Controllers\Controller;

class JSONController extends Controller
{

    private $auth_parameters;
    private $parameters;
    private $createdWithAuthParameters = null;
    private $validAuthParameters = false;
    private $error = [];
    private $actualUser = null;

    function __construct() {
        if (\Auth::check()) {
            $this->actualUser = \Auth::user();
        } else {
            array_push($this->error, 'El usuario no está autenticado');
        }
    }

    /**
     * @return array
     */

    public function getError() {
        return $this->error;
    }

    /**
     * @return mixed
     */

    public function getParameters() {
        return $this->parameters;
    }

    /**
     * @param string $parameter
     * @return mixed
     */

    public function getAuthParameter($parameter = null) {
        if (! is_null($parameter) && isset($this->auth_parameters->$parameter)) {
            return $this->auth_parameters->$parameter;
        }
        if ($this->validAuthParameters) {
            return $this->auth_parameters;
        }
        return null;
    }

    /**
     * @param  string $jsonToParse
     * @return bool
     */

    public function create($jsonToParse) {
        $parsedJson = json_decode($jsonToParse);

        if (! isset($parsedJson->auth_parameters) || ! isset($parsedJson->parameters)) {
            return false;
        }

        $this->auth_parameters = $parsedJson->auth_parameters;
        $this->parameters = $parsedJson->parameters;

        $this->createdWithAuthParameters = true;

        $this->validAuthParameters = $this->checkAuthParameters();
        return $this->validAuthParameters;
    }

    /**
     * @param $jsonToParse
     * @return bool
     */

    public function createWithoutAuthParameters($jsonToParse) {
        $parsedJson = json_decode($jsonToParse);
        if (! isset($parsedJson->parameters)) {
            return false;
        }
        $this->parameters = $parsedJson->parameters;

        $this->createdWithAuthParameters = false;
        $this->validAuthParameters = true;

        return true;
    }

    /**
     * Checks the parameters with the authenticated user
     *
     * @return bool
     */

    private function checkAuthParameters() {
        if (! is_null($this->actualUser)) {
            return $this->checkAuthToken() && $this->checkIdUser() && $this->checkIdUserType();
        }
        return false;
    }

    /**
     * @return bool
     */

    private function checkAuthToken() {
        if (! $this->actualUser->validateAuthToken($this->auth_parameters->auth_token)) {
            return 0 == array_push($this->error, 'Auth token inválido.');
        }
        return true;
    }

    /**
     * @return bool
     */

    private function checkIdUser() {
        if ($this->auth_parameters->id_user != $this->actualUser->id) {
            return 0 == array_push($this->error, 'ID usuario inválido.');
        }
        return true;
    }

    /**
     * @return bool
     */

    private function checkIdUserType() {
        if ($this->auth_parameters->id_user_type != $this->actualUser->user_type) {
            return 0 == array_push($this->error, 'ID user type inválido');
        }
        return true;
    }
}