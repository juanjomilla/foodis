<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 24/11/2018
 * Time: 23:10
 */

namespace App\Http\Controllers\Localization;


class DirectionController
{
    private $street;
    private $street_number;
    private $city;
    private $province;
    private $mapPoint;

    function __construct($street, $street_number, $city, $province, $mapPoint) {
        $this->street = $street;
        $this->street_number = $street_number;
        $this->city = $city;
        $this->province = $province;
        $this->mapPoint = $mapPoint;
    }
}