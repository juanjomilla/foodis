<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 24/11/2018
 * Time: 23:11
 */

namespace App\Http\Controllers\Localization;

class PointMap
{
    private $lat;
    private $long;
    private $earth = 6371;

    public function __construct($lat, $long){
        $this->lat = deg2rad($lat);
        $this->long = deg2rad($long);
    }

    public function distanceToPoint($point) {
        $dlong = $point->long - $this->long;
        $dlat = $point->lat - $this->lat;

        $sinlat = sin($dlat/2);
        $sinlong = sin($dlong/2);

        $a = ($sinlat * $sinlat) + cos($this->lat) * cos($point->lat) * ($sinlong * $sinlong);
        $c = 2 * asin(min(1, sqrt($a)));
        return round($this->earth * $c, 2);
    }
}