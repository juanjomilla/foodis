<?php
namespace App\Http\Controllers\Mail;

use App\ConstEnums\UserTypeEnum;
use App\Mail\SuccessfullyFinishedRegister;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function send()
    {
        $emailDelivery = new \stdClass();
        $emailDelivery->name = "Un nombre";
        $emailDelivery->last_name = "Un apellido";
        $emailDelivery->user_type = 'Delivery';
        $emailDelivery->sender = 'El equipo de foodis';
        $emailDelivery->receiver = "Un apellido," . " un nombre";

        Mail::to("juanjomilla@gmail.com")->send(new SuccessfullyFinishedRegister($emailDelivery));
    }

    static function sendRegistered($user) {
        $emailToSend = new \stdClass();
        $emailToSend->sender = 'El equipo de foodis';

        switch($user->user_type) {
            case UserTypeEnum::Commerce:
                $emailToSend->userName = $user->user_name;
                $emailToSend->user_type = 'Comercio';
                $emailToSend->optional_message = 'Queremos que sepas que con Foodis, tu comercio va a crecer exponencialmente.';
                break;
            case UserTypeEnum::Delivery:
                $emailToSend->userName = $user->user_name;
                $emailToSend->user_type = 'Delivery';
                $emailToSend->optional_message = 'Queremos que sepas que con Foodis, vas a trabajar cuando quieras.';
                break;

            case UserTypeEnum::Client:
                $emailToSend->userName = $user->user_name;
                $emailToSend->user_type = 'Cliente';
                $emailToSend->optional_message = 'Queremos que sepas que no te vas a arrepentir de usar Foodis.';
                break;
        }
        Mail::to($user->email)->send(new SuccessfullyFinishedRegister($emailToSend));
    }
}