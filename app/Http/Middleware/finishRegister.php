<?php

namespace App\Http\Middleware;

use Closure;
use App\ConstEnums\UserTypeEnum;

class finishRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()) {
            $user = \Auth::user();
            if($user->register_finished == 1) {
                return $next($request);
            } else {
                $redirect = redirect('/home');
                switch($user->user_type) {
                    case UserTypeEnum::Commerce:
                        $redirect = redirect('/commerce/finishRegister');
                        break;
                    case UserTypeEnum::Client:
                        $redirect = redirect('/client/finishRegister');
                        break;
                    case UserTypeEnum::Delivery:
                        $redirect = redirect('/delivery/finishRegister');
                        break;
                }
                return $redirect;
            }
        } else {
            return $next($request);
        }
    }
}
