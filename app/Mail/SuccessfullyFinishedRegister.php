<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SuccessfullyFinishedRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('foodis@juanjomilla.com.ar')
            ->view('mail.successfullyRegistered')
            ->text('mail.successfullyRegistered_plain');
        /*
            ->with(
                [
                    'testVarOne' => '1',
                    'testVarTwo' => '2',
                ]);

        /*
         * ->attach(public_path('/images').'/demo.jpg', [
                'as' => 'demo.jpg',
                'mime' => 'image/jpeg',
            ])
         * */
    }
}
