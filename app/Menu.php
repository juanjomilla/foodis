<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = [
        'id', 'id_commerce', 'menu_name', 'description', 'price', 'url_img'
    ];
}
