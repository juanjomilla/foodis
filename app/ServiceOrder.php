<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrder extends Model
{
    protected $table = 'service_order';

    protected $fillable = [
        'id', 'id_state', 'id_client', 'id_commerce', 'id_delivery'
    ];

    public function getState() {
        return $this->belongsTo('App\ServiceOrderState', 'id');
    }
}
