<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderClearing extends Model
{
    protected $table = 'service_order_clearing';

    protected $fillable = [
        'id', 'id_service_order', 'pay_to_commerce', 'pay_to_delivery', 'delivered_late', 'picked_up_late', 'cleared'
    ];
}
