<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderDetails extends Model
{
    protected $table = 'service_order_details';

    protected $fillable = [
        'id', 'id_service_order', 'id_menu', 'qty', 'price'
    ];
}
