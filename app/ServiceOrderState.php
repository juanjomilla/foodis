<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderState extends Model
{
    protected $table = 'service_order_state';

    protected $fillable = [
        'id', 'text'
    ];

    public function getServiceOrder() {
        return $this->hasMany('ServiceOrder', 'id_state');
    }
}
