<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password', 'user_type', 'auth_token', 'register_finished'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function generateAuthToken() {
        $generated_auth_token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $this->auth_token = $generated_auth_token;
        $this->save();
    }

    public function validateAuthToken($tokenToValidate) {
        return $this->auth_token == $tokenToValidate;
    }

    public function finishRegister() {
        $this->register_finished = 1;
        $this->save();
    }
}
