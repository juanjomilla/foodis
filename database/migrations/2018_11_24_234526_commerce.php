<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Commerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commerce', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('commerce_name');
            $table->string('cuit');
            $table->string('street');
            $table->string('street_number');
            $table->string('city');
            $table->string('province');
            $table->decimal('lat', 10,8);
            $table->decimal('long', 11, 8);
            $table->string('delivery_radio');
            $table->integer('minutes_to_deliver')->default('38');
            $table->boolean('enabled')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
