<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Delivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('delivery_name');
            $table->string('delivery_last_name');
            $table->string('dni')->unique();
            $table->string('street');
            $table->string('street_number');
            $table->string('city');
            $table->string('province');
            $table->string('vehicle');
            $table->boolean('picked_up_late')->default(0);
            $table->boolean('enabled')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
