<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_client');
            $table->integer('id_commerce');
            $table->integer('id_delivery')->nullable();
            $table->integer('id_state')->default(1);
            $table->decimal('total_price', 8, 2);
            $table->time('picked_up_at')->nullable();
            $table->time('shipped_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
