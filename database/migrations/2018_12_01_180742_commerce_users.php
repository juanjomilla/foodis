<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommerceUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commerce_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_commerce');
            $table->string('user_name');
            $table->string('client_last_name');
            $table->string('dni')->unique();
            $table->string('street');
            $table->string('street_number');
            $table->string('city');
            $table->string('province');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
