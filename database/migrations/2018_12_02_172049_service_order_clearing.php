<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceOrderClearing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_order_clearing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_service_order');
            $table->decimal('pay_to_commerce', '8', '2')->nullable();
            $table->decimal('pay_to_delivery', '8', '2')->nullable();
            $table->boolean('delivered_late')->default(0);
            $table->boolean('picked_up_late')->default(0);
            $table->boolean('cleared')->default(0);
            $table->date('cleared_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
