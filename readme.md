<p align="center"><img src="https://www.unlam.edu.ar/imagenes/logos/unlam_print.gif"></p>

## Programación web II - Tecnicatura universitaria en desarrollo web

Bienvenidos al repositorio del proyecto final de la materia programación web II

## Branches

En el repositorio se encuentran 2 ramas principales
- **master**: se encuentra el proyecto en etapa funcional, y además está aprobado por el profesor.
- **dev**: se encuentra el proyecto en etapa funcional, pero pendiente de aprobación por el profesor.

Luego cada desarrollador del proyecto tiene su branch con su nombre.

## Sub branches

Para crear sub branches seguir el siguiente formato:

&lt;nombre-branch-padre&gt;/&lt;nombre-sub-branch&gt;

**Ejemplo:**
Al desarrollador Juanjo le toca hacer el login del proyecto, entonces voy a crear una sub branch de mi branch propia. Y como la funcionalidad es agregar el login, creo la branch de esta manera:

**$ git checkout -b juanjo/login**

## Laravel
El proyecto va a estar basado en el framework **Laravel** en su versión 5.4.30

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

Su documentación se encuentra aquí:
<p align="center">
<a href="https://laravel.com/docs/5.4">Documentación de Laravel 5.4</a>
</p>