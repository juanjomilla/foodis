@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/shoppingcart.css" rel="stylesheet">
@endsection
@section('title', 'Deliveries')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info active" id="inactivosLink">
            Inactivos
          </button>
          <button class="btn btn-info" id="activosLink">
            Activos
          </button>
        </div>
      </div>
    </div>
  </div> 
  <h3>Deliveries</h3>
  <div class="row" id="inactivos" style="display: block;">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="inactivosTable">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellido</th>
              <th scope="col">DNI</th>
              <th scope="col">Dirección</th>
              <th scope="col">Ciudad</th>
              <th></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="activos" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="activosTable">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellido</th>
              <th scope="col">DNI</th>
              <th scope="col">Dirección</th>
              <th scope="col">Ciudad</th>
              <th></th>            
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
  $(function() {
    $('#inactivosLink').click(function(e) {
      $("#inactivos").delay(100).fadeIn(100);
      $("#activos").fadeOut(100);
      $('#activosLink').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });

    $('#activosLink').click(function(e) {
      $("#activos").delay(100).fadeIn(100);
      $("#inactivos").fadeOut(100);
      $('#inactivosLink').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });

  });

  function enableDelivery(id_delivery){
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_delivery" : id_delivery
      }
    };

    dataJson =  JSON.stringify(data);

    $.ajax({
      type: "POST",
      url: "{{url('admin/enableDelivery')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        if(data["succes"])
        {
          location.reload();
        }
        else{
          console.log(data);
        }  
      }
    });

  }


  function disableDelivery(id_delivery){
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_delivery" : id_delivery
      }
    };

    dataJson =  JSON.stringify(data);


    $.ajax({
      type: "POST",
      url: "{{url('admin/disableDelivery')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        if(data["succes"])
        {
          location.reload();
        }
        else{
          console.log(data);
        }  
      }
    });

  }

</script>
@endsection
@section('ajax')
<script src="{{url('')}}/js/datatables.min.js"></script>
<script>
  $(document).ready(function(){
   var dataInactivo =
   {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
    }
  };

  dataInactivoJson =  JSON.stringify(dataInactivo);

  $('#inactivosTable').DataTable({
    "ajax":{
      "type": "POST",
      "url": "{{url('/admin/getDeliveriesDisabled')}}",
      "data": {data_request: dataInactivoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id_delivery"},
    {"data":"name"},
    {"data":"last_name"},
    {"data":"cuit"},
    {"data":"direction"},
    {"data":"city"},
    {"data":"null", render: function(data,type,row,meta){
      if(data==null){
        return '<a class="btn btn-primary text-white" onClick="enableDelivery('+row.id_delivery+')">Habilitar</a>';
      }
    }
  }
  ]
});

  var dataActivo =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
    }
  };

  dataActivoJson =  JSON.stringify(dataActivo);

  $('#activosTable').DataTable({
    "ajax":{
      "type": "POST",
      "url": "{{url('/admin/getDeliveriesEnabled')}}",
      "data": {data_request: dataActivoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id_delivery"},
    {"data":"name"},
    {"data":"last_name"},
    {"data":"cuit"},
    {"data":"direction"},
    {"data":"city"},
    {"data":"null", render: function(data,type,row,meta){
      if(data==null){
        return '<a class="btn btn-primary text-white" onClick="disableDelivery('+row.id_delivery+')">Deshabilitar</a>';
      }
    }
  }
  ]
});


});
</script>
@endsection