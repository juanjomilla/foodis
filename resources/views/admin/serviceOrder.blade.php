@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/shoppingcart.css" rel="stylesheet">
@endsection
@section('title', 'Liquidaciones')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info active" id="pendienteLink">
            Pendiente de liquidación
          </button>
          <button class="btn btn-info" id="LiquidadasLink">
            Liquidadas
          </button>
        </div>
      </div>
    </div>
  </div> 
  <h3>Liquidaciones</h3>
  <button class="btn btn-info" onclick="makeClearing()">
    Liquidar pendientes
  </button>
  <div class="row" id="liquidacionesPendientes" style="display: block;">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="liquidacionesPendientesTable">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Pago al comercio</th>
              <th scope="col">Pago al delivery</th>
              <th scope="col">Tardó en recoger</th>
              <th scope="col">Tardó en entregar</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="liquidaciones" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="liquidacionesTable">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Pago al comercio</th>
              <th scope="col">Pago al delivery</th>
              <th scope="col">Tardó en recoger</th>
              <th scope="col">Tardó en entregar</th>
              <th scope="col">Fecha de liquidación</th>              
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
  $(function() {
    $('#pendienteLink').click(function(e) {
      $("#liquidacionesPendientes").delay(100).fadeIn(100);
      $("#liquidaciones").fadeOut(100);
      $('#LiquidadasLink').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });

    $('#LiquidadasLink').click(function(e) {
      $("#liquidaciones").delay(100).fadeIn(100);
      $("#liquidacionesPendientes").fadeOut(100);
      $('#pendienteLink').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });

  });

  function makeClearing(){
        var data =
      {
        "auth_parameters":
        {
          "id_user": {{Auth::user()->id}},
          "id_user_type" : {{Auth::user()->user_type}},
          "auth_token" : "{{Auth::user()->auth_token}}",
        },
        "parameters":
        {
        }
      };

      dataJson =  JSON.stringify(data);
      console.log(dataJson);

      $.ajax({
        type: "POST",
        url: "{{url('admin/makeClearing')}}",
        data: {data_request: dataJson, _token: "{{csrf_token()}}"},
        dataType: "json",
        cache:false,
        success:
        function(data){
        console.log(data);
        if(data["success"])
        {
        location.reload();
        }
        else{
          console.log(data);
        }  
        }
      });
  }
</script>
@endsection
@section('ajax')
<script src="{{url('')}}/js/datatables.min.js"></script>
<script>
  $(document).ready(function(){
   var dataPendiente =
   {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
    }
  };

  dataPendienteJson =  JSON.stringify(dataPendiente);
  console.log(dataPendienteJson);

  $('#liquidacionesPendientesTable').DataTable({
    "ajax":{
      "type": "POST",
      "url": "{{url('/admin/getCompletedServiceOrderWithoutClearing')}}",
      "data": {data_request: dataPendienteJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id_service_order"},
    {"data":"pay_to_commerce"},
    {"data":"pay_to_delivery"},
    {"data":"picked_up_late", render: function(data,type,row,meta){
      if(data==1){
        return 'Sí';
      }else{return 'No';}
    }
  },
  {"data":"delivered_late", render: function(data,type,row,meta){
    if(data==1){
      return 'Sí';
    }else{return 'No';}
  }
}
]
});

  var dataLiquidada =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
    }
  };

  dataLiquidadaJson =  JSON.stringify(dataLiquidada);
  console.log(dataLiquidadaJson);

  $('#liquidacionesTable').DataTable({
    "ajax":{
      "type": "POST",
      "url": "{{url('/admin/getCompletedServiceOrderWithClearing')}}",
      "data": {data_request: dataLiquidadaJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id_service_order"},
    {"data":"pay_to_commerce"},
    {"data":"pay_to_delivery"},
    {"data":"picked_up_late", render: function(data,type,row,meta){
      if(data==1){
        return 'Sí';
      }else{return 'No';}
    }
  },
  {"data":"delivered_late", render: function(data,type,row,meta){
    if(data==1){
      return 'Sí';
    }else{return 'No';}
  }
},
{"data":"cleared_date"}
]
});


});
</script>
@endsection