@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/registerButtons.css" rel="stylesheet">
@endsection
@section('title', 'Registrarse')
@section('maincontent')
<div align="center">
    <div>
        <div class="container">
            <a class="intro-block image-register-button" href="{{url('/client/register')}}" style="background-image:url('img/client.jpg')">
              <div class="intro-block-inner">
                <h2>Registrar como usuario</h2>
            </div>
        </a>
    </div>
    <div class="container">
       <a class="intro-block image-register-button" href="{{url('/commerce/register')}}" style="background-image:url('img/commerce.jpg')">
          <div class="intro-block-inner">
            <h2>Registrar como comercio</h2>
        </div>
    </a>
</div>
<div class="container">
  <a class="intro-block image-register-button" href="{{url('/delivery/register')}}" style="background-image:url('img/delivery.jpg')">
      <div class="intro-block-inner">
        <h2 class="buttonText">Registrar como delivery</h2>
    </div>
</a>
</div>

</div>
</div>
@endsection