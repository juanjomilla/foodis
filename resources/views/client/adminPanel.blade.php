@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/showMenues.css" rel="stylesheet">
@endsection
@section('title', 'Panel de administración')
@section('maincontent')
  <div class="row">
    <div class="col-sm-3"><!--left col-->
      <div class="text-center">
        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
        <h6></h6>
        <input type="file" class="text-center center-block file-upload">
      </div><br> 
    </div><!--/col-3-->

      <div class="tab-content">
        <div class="tab-pane active" id="home">
          <hr>
          <div class="form-group">

              <div class="col-xs-6">
                <label for="first_name"><h4>Nombre</h4></label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
              </div>
            </div>
            <div class="form-group">

              <div class="col-xs-6">
                <label for="last_name"><h4>Apellido</h4></label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
              </div>
            </div>

            <div class="form-group">

              <div class="col-xs-6">
                <label for="phone"><h4>Dirección</h4></label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Calle" title="enter your phone number if any.">
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Altura" title="enter your phone number if any.">
              </div>
            </div>
          <div class="form-group">
           <div class="col-xs-12">
            <br>
            <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i>Guardar</button>
          </div>
        </div>

      <hr>


</div><!--/tab-pane-->
</div><!--/tab-content-->

</div><!--/col-9-->
</div><!--/row-->

@endsection
@section('js')
@endsection
@section('ajax')
<script>

  $(document).ready(function(){

    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
      }
    };

    dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('client/showCommerce')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){

      }
    });
  });
</script>
@endsection