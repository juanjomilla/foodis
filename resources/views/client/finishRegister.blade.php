@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/registerForm.css" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
@endsection
@section('title', 'Terminar registro')
@section('maincontent')
<div class="container">
  <div class="form-horizontal" role="form">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <h2>Finalizar registro</h2>
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="client_name">Nombre</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
            <input type="text" name="client_name" class="form-control" id="client_name"
            placeholder="Nombre" required autofocus>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="client_last_name">Apellido</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"></div>
            <input type="text" name="client_last_name" class="form-control" id="client_last_name"
            placeholder="Apellido" required autofocus>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="dni">DNI</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"></div>
            <input type="text" name="dni" class="form-control" id="dni"
            placeholder="DNI" required autofocus>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="provincia">Provincia</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
            <select name="provincia" id="province" class="form-control">
              <option value="Buenos Aires">Bs. As.</option>
              <option value="Catamarca">Catamarca</option>
              <option value="Chaco">Chaco</option>
              <option value="Chubut">Chubut</option>
              <option value="Cordoba">Cordoba</option>
              <option value="Corrientes">Corrientes</option>
              <option value="Entre Rios">Entre Rios</option>
              <option value="Formosa">Formosa</option>
              <option value="Jujuy">Jujuy</option>
              <option value="La Pampa">La Pampa</option>
              <option value="La Rioja">La Rioja</option>
              <option value="Mendoza">Mendoza</option>
              <option value="Misiones">Misiones</option>
              <option value="Neuquen">Neuquen</option>
              <option value="Rio Negro">Rio Negro</option>
              <option value="Salta">Salta</option>
              <option value="San Juan">San Juan</option>
              <option value="San Luis">San Luis</option>
              <option value="Santa Cruz">Santa Cruz</option>
              <option value="Santa Fe">Santa Fe</option>
              <option value="Sgo. del Estero">Sgo. del Estero</option>
              <option value="Tierra del Fuego">Tierra del Fuego</option>
              <option value="Tucuman">Tucuman</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="city">Ciudad</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
            <input type="text" class="form-control" name="city" id="city" placeholder="Ciudad">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="city">Dirección</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
            <input type="text" name="street" class="form-control" id="street" placeholder="Calle">
            <input type="number" name="street_number" class="form-control" id="street_number" placeholder="Altura">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 field-label-responsive">
        <label for="address">Buscar en el mapa</label>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
            <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
            <input type="text" name="address" id="address">
            <input type="button" name="searchAddress" id="searchAddress" class="form-control" value="Buscar">
            <input type="text" name="lat" id="lat" disabled class="form-control">
            <input type="text" name="long" id="long" disabled class="form-control">
          </div>
        </div>
      </div>
    </div>
    <div id="map" style="width: 300px; height: 300px">
    </div>

  </div>
  <!-- Dirección, latitud, longitud, radio de acción -->
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <button id="finishRegister" class="btn btn-success"><i class="fa fa-user-plus"></i> Terminar registro </button>
      <span id="spanerror" class="text-danger"></span>
    </div>
  </div>
</div>
@endsection
@section('js')
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1542186754" />
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>


<script>
// Initialize the platform object:
function geocode(platform, address) {
  var geocoder = platform.getGeocodingService(),
  geocodingParameters = {
    searchText: address,
    jsonattributes : 1
  };

  geocoder.geocode(
    geocodingParameters,
    onSuccess,
    onError
    );
}
/**
 * This function will be called once the Geocoder REST API provides a response
 * @param  {Object} result          A JSONP object representing the  location(s) found.
 *
 * see: http://developer.here.com/rest-apis/documentation/geocoder/topics/resource-type-response-geocode.html
 */
 function onSuccess(result) {
  var locations = result.response.view[0].result;
 /*
  * The styling of the geocoding response on the map is entirely under the developer's control.
  * A representitive styling can be found the full JS + HTML code of this example
  * in the functions below:
  */
  addLocationsToMap(locations);
  // ... etc.
}

/**
 * This function will be called if a communication error occurs during the JSON-P request
 * @param  {Object} error  The error message received.
 */
 function onError(error) {
  alert('Ooops!');
}




/**
 * Boilerplate map initialization code starts below:
 */

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
  'app_id': 'rJiRmujvggTUctAQdVcz',
  'app_code': 'cGTBiKr-8RViLHiufJVeCA',
  useHTTPS: true
});
var pixelRatio = window.devicePixelRatio || 1;
var defaultLayers = platform.createDefaultLayers({
  tileSize: pixelRatio === 1 ? 256 : 512,
  ppi: pixelRatio === 1 ? undefined : 320
});

//Step 2: initialize a map - this map is centered over California
var map = new H.Map(document.getElementById('map'),
  defaultLayers.normal.map,{
    center: {lat:-34.608591835694774, lng:-58.37361166137694},
    zoom: 15,
    pixelRatio: pixelRatio
  });

var locationsContainer = document.getElementById('panel');

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);


/**
 * Creates a series of H.map.Markers for each location found, and adds it to the map.
 * @param {Object[]} locations An array of locations as received from the
 *                             H.service.GeocodingService
 */
 function addLocationsToMap(locations){
  var group = new  H.map.Group(),
  position,
  i;

  // Add a marker for each location found
  for (i = 0;  i < locations.length; i += 1) {
    position = {
      lat: locations[i].location.displayPosition.latitude,
      lng: locations[i].location.displayPosition.longitude
    };
    marker = new H.map.Marker(position);
    marker.label = locations[i].location.address.label;
    marker.draggable = true;
    group.addObject(marker);
    $("#lat").val(position.lat);
    $("#long").val(position.lng);
  }

  group.addEventListener('tap', function (evt) {
    map.setCenter(evt.target.getPosition());
  }, false);

  // Add the locations group to the map
  map.addObject(group);
  map.setCenter(group.getBounds().getCenter());

  // disable the default draggability of the underlying map
  // when starting to drag a marker object:
  map.addEventListener('dragstart', function(ev) {
    var target = ev.target;
    if (target instanceof H.map.Marker) {
      behavior.disable();
    }
  }, false);


  // re-enable the default draggability of the underlying map
  // when dragging has completed
  map.addEventListener('dragend', function(ev) {
    var target = ev.target;
    if (target instanceof mapsjs.map.Marker) {
      behavior.enable();
    }
  }, false);

  // Listen to the drag event and move the position of the marker
  // as necessary
  map.addEventListener('drag', function(ev) {
    var target = ev.target,
    pointer = ev.currentPointer;
    var coord = map.screenToGeo(ev.currentPointer.viewportX,
      ev.currentPointer.viewportY);
    if (target instanceof mapsjs.map.Marker) {
      target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
    }
    $("#lat").val(coord.lat);
    $("#long").val(coord.lng);

  }, false);
}


// Now use the map as required...

$( "#searchAddress" ).click(function() {
  var address = $("#address").val();
  geocode(platform, address);
});

</script>
@endsection
@section('ajax')
<script>
  $(document).ready(function(){

    $("#finishRegister").on("click", function(){
      var client_name = $("#client_name").val();
      var client_last_name = $("#client_last_name").val();
      var dni = $("#dni").val();
      var province = $("#province").val();
      var city = $("#city").val();
      var street = $("#street").val();
      var street_number = $("#street_number").val();
      var lat = $("#lat").val();
      var long = $("#long").val();
      var data =
      {
        "auth_parameters":
        {
          "id_user": {{Auth::user()->id}},
          "id_user_type" : {{Auth::user()->user_type}},
          "auth_token" : "{{Auth::user()->auth_token}}",
        },
        "parameters":
        {
          "client_name" : client_name,
          "client_last_name" : client_last_name,
          "dni" : dni,
          "street" : street,
          "street_number" : street_number,
          "city" : city,
          "province" : province,
          "lat" : lat,
          "long" : long
        }
      };

      dataJson =  JSON.stringify(data);
      console.log(dataJson);

      $.ajax({
        type: "POST",
        url: "{{url('client/finishRegister')}}",
        data: {data_request: dataJson, _token: "{{csrf_token()}}"},
        dataType: "json",
        cache:false,
        success:
        function(data){
        console.log(data);
        if(data["success"])
        {
        window.location.href = "{{url('')}}"; 
        }
        else{
          $("#spanerror").html("error código "+data["error"].code+"");
        }  
        }
      });
    });
  });
</script>
@endsection
