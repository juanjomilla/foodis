@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/showMenues.css" rel="stylesheet">
@endsection
@section('title', 'Menú')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info" id="list">
            Lista
          </button>
          <button class="btn btn-danger" id="grid">
            Cuadrícula
          </button>
        </div>
      </div>
    </div>
  </div> 
  <div id="products" class="row view-group">

  </div>
</div>
@endsection
@section('js')
<script>
  $(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
  });


</script>
@endsection
@section('ajax')
<script>

  $(document).ready(function(){

    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
      }
    };

    dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('client/showCommerce')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        $.each(data, function(i, item) {
          $("#products").append('<div class="item col-xs-4 col-lg-4"> <div class="thumbnail card"> <div class="img-event"><img class="group list-group-image img-fluid" src="http://placehold.it/400x250/000/fff" alt="" /> </div> <div class="caption card-body"><h4 class="group card-title inner list-group-item-heading">'+data[i].commerce_name+'</h4> <div class="row"> <div class="col-xs-12 col-md-6"> <a class="btn btn-success" href="{{url("/client/menu")}}/'+data[i].id_commerce+'"">Ver menú</a> </div> </div></div></div></div> ');
        });             
      }
    });
  });
</script>
@endsection