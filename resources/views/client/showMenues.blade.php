@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/showMenues.css" rel="stylesheet">
@endsection
@section('title', 'Menú')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
        <span id="alertCart"></span>
        <span id="agregado"></span>
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info" id="list">
            Lista
          </button>
          <button class="btn btn-danger" id="grid">
            Cuadrícula
          </button>
        </div>
      </div>
    </div>
  </div> 
  <div id="products" class="row view-group">

  </div>
</div>
@endsection
@section('js')
<script>
  $(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
  });
</script>
@endsection
@section('ajax')
<script>

  function verifyCartInProgress()
  {
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
      }
    };
        dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('/client/getIdCommerceFromUserCart')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        if(data!==null){
          if(data!=={{$idCommerce}}){
          console.log("Carrito en otra tienda");
          $("#alertCart").append("<p>Actualmente tiene un carrito en progreso en otro comercio.");
          $("#alertCart").append('<a class="btn btn-lg btn-primary" href="{{url("/client/menu")}}/'+data+'">Ir a tienda</a> ');
         $("#alertCart").append('<a href="" class="btn btn-lg btn-primary" onclick="emptyCart('+{{$idCommerce}}+')">Comprar acá</a> </p>');
             $(".addToCart").addClass("disabled");


        }else{console.log("carrito en esta tienda")}
        }        
      }
    });
  }

  function emptyCart(id)
  {
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "menu_id": id
      }
    };
        dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('/client/emptyCart/')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
       console.log(data);     
      }
    });
  }

  function addToCart(id, menuName) {              
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_commerce": {{$idCommerce}},
        "id_menu" : id,
        "qty" : "1"
      }
    };
        dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('/client/addToCart')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        console.log(data);
        $("#agregado").empty();
        $("#agregado").append("Agregado correctamente");           
      }
    });
  }
  $(document).ready(function(){
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_commerce": {{$idCommerce}}
     }
    };

    dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('/menuFromCommerce')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        $.each(data, function(i, item) {
          $("#products").append('<div class="item col-xs-4 col-lg-4"> <div class="thumbnail card"> <div class="img-event"><img class="group list-group-image img-fluid" src="{{url("/resources/storage/img/menu")}}/'+data[i].url_img+'" alt="" style="max-width:100%; height:auto;" /> </div> <div class="caption card-body"><h4 class="group card-title inner list-group-item-heading">'+data[i].menu_name+'</h4><p class="group inner list-group-item-text">'+data[i].description+'</p><div class="row"><div class="col-xs-12 col-md-6"><p class="lead">$'+data[i].price+'</p></div><div class="col-xs-12 col-md-6"><a class="btn btn-success addToCart" onclick="addToCart('+data[i].menu_id+')"><i class="fas fa-shopping-cart"></i> Agregar</a></div></div></div></div></div> ');
        });
        verifyCartInProgress();             
      }
    });
  });
</script>
@endsection