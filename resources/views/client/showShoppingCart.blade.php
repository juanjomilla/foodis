@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/shoppingcart.css" rel="stylesheet">
@endsection
@section('title', 'Carrito')
@section('maincontent')

<div class="container mb-4">
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped" id="cartTable">
          <thead>
            <tr>
              <th scope="col"> </th>
              <th scope="col">Producto</th>
              <th scope="col" class="text-center">Cantidad</th>
              <th scope="col" class="text-right">Precio</th>
              <th> </th>
            </tr>
          </thead>
          <tbody id="bodyCart">

          </tbody>
        </table>
      </div>
    </div>
    <div class="col mb-2">
      <div class="row">
        <div class="col-sm-12  col-md-6">
          <a class="btn btn-block btn-light" href="{{url('/client/commerces')}}">Continuar comprando</a>
        </div>
        <div class="col-sm-12 col-md-6 text-right">
          <button class="btn btn-lg btn-block btn-success text-uppercase" onclick="confirmCart()">Confirmar compra</button>
          <span id="spanerror"></span>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>

</script>
@section('ajax')
<script>

  function updateItemQty(id){
  var id_menu = id;
  var qty = $("#"+ id_menu).val();
   var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_menu" : id_menu,
        "qty" : qty
      }
    };
    dataJson =  JSON.stringify(data);
    console.log(dataJson);

    var regexpnumbers = new RegExp("^(?:[1-9]|0[1-9]|10)$");
    if(regexpnumbers.test(qty)){
    $.ajax({
      type: "POST",
      url: "{{url('/client/updateItemQty')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        console.log(data);
        if(data["success"])
        {
          alert("modificado correctamente");
        }
        else{
          $("#spanerror").html("error código "+data["error"].code+"");
        }        
      }
    });
  }
  else{
    alert("Cantidad mínima 1, máxima 10. Ingresaste" + qty);
  }
  }


  function removeItem(id){
  var id_menu = id;
  var qty = $("#"+ id_menu).val();
   var dataRemove =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "id_menu" : id_menu,
        "qty" : 0
      }
    };
    dataRemoveJson =  JSON.stringify(dataRemove);
    console.log(dataRemoveJson);

    var regexpnumbers = new RegExp("^(?:[1-9]|0[1-9]|10)$");
    if(regexpnumbers.test(qty)){
    $.ajax({
      type: "POST",
      url: "{{url('/client/updateItemQty')}}",
      data: {data_request: dataRemoveJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        console.log(data);
        if(data["success"])
        {
          location.reload();
        }
        else{
          $("#spanerror").html("error código "+data["error"].code+"");
        }        
      }
    });
  }
  else{
    alert("No deberías llegar hasta esta alerta.");
  }
  }


  function getTotalPrice(){

    var totalPrice = 0;
    $('#cartTable > tbody > .tr').each(function() {
      var qty = $(this).find(".qty").val();    
      var price = $(this).find(".price").html();
      var total = qty * price;
      totalPrice += total;
    });
    $("#totalPrice").html(totalPrice);
  }

  function confirmCart() {
  var total_price = $("#totalPrice").html();
    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
        "total_price" : total_price,
      }
    };
    dataJson =  JSON.stringify(data);
    console.log(dataJson);

    $.ajax({
      type: "POST",
      url: "{{url('/client/confirmCart')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        console.log(data);
        if(data["success"])
        {
        window.location.href = "{{url('client/serviceOrder')}}"; 
        }
        else{
          $("#spanerror").html("error código "+data["error"].code+"");
        }        
      }
    });
  }
  $(document).ready(function(){

    var data =
    {
      "auth_parameters":
      {
        "id_user": {{Auth::user()->id}},
        "id_user_type" : {{Auth::user()->user_type}},
        "auth_token" : "{{Auth::user()->auth_token}}",
      },
      "parameters":
      {
      }
    };

    dataJson =  JSON.stringify(data);
    console.log(dataJson);
    $.ajax({
      type: "POST",
      url: "{{url('/client/showCart')}}",
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        $.each(data, function(i, item) {
          $("#bodyCart").append('<tr class="tr"><td><img src="{{url("/resources/storage/img/menu")}}/'+data[i].url_img+'" style="width: 50px; height:50px;" /> </td><td>'+data[i].menu_name+'</td><td class="input-group"><input id="'+data[i].id_menu+'" class="form-control qty" type="number" min="1" max="10" value="'+data[i].qty+'"/><button class="btn btn-sm btn-success" onclick="updateItemQty('+data[i].id_menu+')">Modificar cantidad <i class="fa fa-edit"></i></button></td><td class="text-right price" id="price">'+data[i].price+'</td><td class="text-right"><button class="btn btn-sm btn-danger" onclick="removeItem('+data[i].id_menu+')"><i class="fa fa-trash"></i> </button> </td></tr>');
        }); 
        $("#bodyCart").append('<tr><td></td><td></td><td></td><td>Total</td class="text-right"><strong>total</strong><td id="totalPrice"></td></tr>');
        getTotalPrice();
      }
    });
  });

$("body").on('change', '.qty', function(){
    getTotalPrice();
});

</script>

@endsection