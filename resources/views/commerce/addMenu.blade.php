@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/addMenu.css" rel="stylesheet">
<link href="{{url('')}}/css/registerForm.css" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
@endsection
@section('title', 'Agregar menú')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <h2>Agregar menú</h2>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="menu_name">Nombre</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="menu_name" class="form-control" id="menu_name"
          placeholder="Nombre" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="description">Descripción</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="description" class="form-control" id="description"
          placeholder="Descripción" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="price">Precio</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="number" name="price" class="form-control" id="price"
          placeholder="Precio" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="imagen">Imagen</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input class="form-control" id="picture" name="picture" type='file' onchange="readURL(this);" />
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label></label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <img id="img-preview" src="http://placehold.it/180" alt="your image" />
          <input id="b64" type="hidden">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <input class="btn btn-primary form-control" type="submit" id="confirmMenu" name="confirmMenu" value="Comfirmar"/>
    <span id="spanerror" class="text-danger"></span>
  </div>
</div>
</div>
@endsection
@section('js')
<script>
 function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#menuImage')
      .attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endsection
@section('ajax')

<script>
  $(document).ready(function(){

    $("#confirmMenu").click(function(){

      var data =
      {
        "auth_parameters":
        {
          "id_user": {{Auth::user()->id}},
          "id_user_type" : {{Auth::user()->user_type}},
          "auth_token" : "{{Auth::user()->auth_token}}",
        },
        "parameters":
        {
          "menu_name" : $("#menu_name").val(),
          "description" : $("#description").val(),
          "price" : $("#price").val(),
          "base64img": $("#b64").attr("value")

        }
      };

      dataJson =  JSON.stringify(data);
      console.log(dataJson);

      $.ajax({
        type: "POST",
        url: "{{url('commerce/saveMenu')}}",
        data: {data_request: dataJson, _token: "{{csrf_token()}}"},
        dataType: "json",
        cache:false,
        success:
        function(data){
          console.log(data);
        if(data["success"])
        {
        window.location.href = "{{url('commerce/myMenu')}}"; 
        }
        else{
          $("#spanerror").html("error código "+data["error"].code+"");
        }  
        }
      });

   });

   $("#picture").on("change", readFile);
   function readFile() {
    if (this.files && this.files[0]) {
      let FR = new FileReader();
      FR.addEventListener("load", function(e) {
        $("#img-preview").attr("src", e.target.result);
        $("#b64").attr("value", e.target.result);
      });
      FR.readAsDataURL( this.files[0] );
    }
  }
});
</script>
@endsection