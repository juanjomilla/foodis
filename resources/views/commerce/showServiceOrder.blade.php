@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/shoppingcart.css" rel="stylesheet">
@endsection
@section('title', 'Pedidos')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info active" id="pendientePagoLink">
            Pendiente de pago
          </button>
          <button class="btn btn-info" id="pagadoLink">
            Pagado
          </button>
          <button class="btn btn-info" id="waitingLink">
            Por recoger
          </button>       
          <button class="btn btn-info" id="enTransitoLink">
            En tránsito
          </button>
          <button class="btn btn-info" id="completoLink">
            Completo
          </button>
        </div>
      </div>
    </div>
  </div> 
  <h3>Órdenes de compra</h3>
  <div class="row" id="mostrarPendiente" style="display: block;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Pendiente de pago</h>
        <table class="table table-striped" id="serviceOrderClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
              <th scope="col"> </th>

            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="mostrarPagado" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Pagado</h>
        <table class="table table-striped" id="serviceOrderPagadoClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="porRecoger" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Esperando ser recogido</h>
        <table class="table table-striped" id="serviceOrderPorRecoger">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>  
  <div class="row" id="mostrarEnTransito" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>En tránsito</h>
        <table class="table table-striped" id="serviceOrderTransitoClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="mostrarCompleto" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Completado</h>
        <table class="table table-striped" id="serviceOrderCompleteClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
 $(function() {
// botones: pendientePagoLink - pagadoLink - enTransitoLink - completoLink
// mostrarPendiente - mostrarPagado - mostrarEnTransito - mostrarCompeto
$('#pendientePagoLink').click(function(e) {
  $("#mostrarPendiente").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#porRecoger").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $('#waitingLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#pagadoLink').click(function(e) {
  $("#mostrarPagado").delay(100).fadeIn(100);
  $("#mostrarPendiente").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $("#porRecoger").fadeOut(100);
  $('#pendientePagoLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $('#waitingLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#enTransitoLink').click(function(e) {
  $("#mostrarEnTransito").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#mostrarPendiente").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $("#porRecoger").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#pendientePagoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $('#waitingLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#completoLink').click(function(e) {
  $("#mostrarCompleto").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#mostrarPendiente").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#porRecoger").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#pendientePagoLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#waitingLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#waitingLink').click(function(e) {
  $("#porRecoger").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#mostrarPendiente").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#pendientePagoLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});
});
</script>
@endsection
@section('ajax')
<script src="{{url('')}}/js/datatables.min.js"></script>
<script>
  $(document).ready(function(){
   var dataPendiente =
   {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "1"
    }
  };

  dataPendienteJson =  JSON.stringify(dataPendiente);
  console.log(dataPendienteJson);

  $('#serviceOrderClient').DataTable({
    "lengthChange": false,
    "bFilter": false,
    "info": false,
    "ajax":{
      "type": "POST",
      "url": "{{url('/commerce/getServiceOrder')}}",
      "data": {data_request: dataPendienteJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
  ]
});

  var dataPagado =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "2"
    }
  };

  dataPagadoJson =  JSON.stringify(dataPagado);
  console.log(dataPagadoJson);

  $('#serviceOrderPagadoClient').DataTable({
    "lengthChange": false,
    "bFilter": false,
        "info": false,
    "ajax":{
      "type": "POST",
      "url": "{{url('/commerce/getServiceOrder')}}",
      "data": {data_request: dataPagadoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
    ]
  });


  var dataPorRecoger =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "3"
    }
  };

  dataPorRecogerJson =  JSON.stringify(dataPorRecoger);
  console.log(dataPorRecogerJson);

  $('#serviceOrderPorRecoger').DataTable({
    "lengthChange": false,
    "bFilter": false,
    "ajax":{
      "type": "POST",
      "url": "{{url('/commerce/getServiceOrder')}}",
      "data": {data_request: dataPorRecogerJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
    ]
  });

  var dataTransito =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "4"
    }
  };

  dataTransitoJson =  JSON.stringify(dataTransito);
  console.log(dataTransitoJson);

  $('#serviceOrderTransitoClient').DataTable({
    "lengthChange": false,
    "bFilter": false,
        "info": false,

    "ajax":{
      "type": "POST",
      "url": "{{url('/commerce/getServiceOrder')}}",
      "data": {data_request: dataTransitoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
    ]
  });


  var dataCompleto =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "5"
    }
  };

  dataCompletoJson =  JSON.stringify(dataCompleto);
  console.log(dataCompletoJson);

  $('#serviceOrderCompleteClient').DataTable({
    "lengthChange": false,
    "bFilter": false,
        "info": false,
    "ajax":{
      "type": "POST",
      "url": "{{url('/commerce/getServiceOrder')}}",
      "data": {data_request: dataCompletoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
    ]
  });


});
</script>
@endsection