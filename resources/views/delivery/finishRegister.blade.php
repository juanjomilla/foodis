@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/registerForm.css" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
@endsection
@section('title', 'Terminar registro')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <h2>Finalizar registro</h2>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="nombre">Nombre</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
          <input type="text" name="delivery_name" class="form-control" id="delivery_name"
          placeholder="Nombre" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="apellido">Apellido</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="apellido" class="form-control" id="lastname"
          placeholder="Apellido" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="dni">DNI</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="dni" class="form-control" id="dni"
          placeholder="DNI" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="province">Provincia</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
          <select name="province" id="province">
            <option value="Buenos Aires">Bs. As.</option>
            <option value="Catamarca">Catamarca</option>
            <option value="Chaco">Chaco</option>
            <option value="Chubut">Chubut</option>
            <option value="Cordoba">Cordoba</option>
            <option value="Corrientes">Corrientes</option>
            <option value="Entre Rios">Entre Rios</option>
            <option value="Formosa">Formosa</option>
            <option value="Jujuy">Jujuy</option>
            <option value="La Pampa">La Pampa</option>
            <option value="La Rioja">La Rioja</option>
            <option value="Mendoza">Mendoza</option>
            <option value="Misiones">Misiones</option>
            <option value="Neuquen">Neuquen</option>
            <option value="Rio Negro">Rio Negro</option>
            <option value="Salta">Salta</option>
            <option value="San Juan">San Juan</option>
            <option value="San Luis">San Luis</option>
            <option value="Santa Cruz">Santa Cruz</option>
            <option value="Santa Fe">Santa Fe</option>
            <option value="Sgo. del Estero">Sgo. del Estero</option>
            <option value="Tierra del Fuego">Tierra del Fuego</option>
            <option value="Tucuman">Tucuman</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="city">Ciudad</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="city" class="form-control" id="city"
          placeholder="Ciudad" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label>Calle</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"></div>
          <input type="text" name="street" class="form-control" id="street"
          placeholder="Calle" required autofocus>
          <input type="number" name="street_number" class="form-control" id="street_number"
          placeholder="Altura" required autofocus>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 field-label-responsive">
      <label for="vehicle">Vehiculo</label>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
          <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-store-alt"></i></div>
          <select name="vehicle" id="vehicle">
            <option value="moto">moto</option>
            <option value="bici">bici</option>
            <option value="auto">auto</option>
          </select>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <input class="btn btn-primary form-control" type="submit" id="finishRegister" name="finishRegister" value="Enviar"/>
    <span id="spanerror" class="text-danger"></span>


  </div>
</div>
</div>
@endsection

@section('ajax')
<script>
  $(document).ready(function(){

    $("#finishRegister").click(function(){
      var name= $("#delivery_name").val();
      var lastname = $("#lastname").val();
      var dni = $("#dni").val();
      var street = $("#street").val();
      var street_number = $("#street_number").val();
      var city = $("#city").val();
      var province = $("#province").val();
      var vehicle = $("#vehicle").val();
      var data =
      {
        "auth_parameters":
        {
          "id_user": {{Auth::user()->id}},
          "id_user_type" : {{Auth::user()->user_type}},
          "auth_token" : "{{Auth::user()->auth_token}}",
        },
        "parameters":
        {
          "delivery_name" : name,
          "delivery_last_name" : lastname,
          "dni" : dni,
          "street" : street,
          "street_number" : street_number,
          "city" : city,
          "province" : province,
          "vehicle" : vehicle
        }
      };

      dataJson =  JSON.stringify(data);
      console.log(dataJson);

      $.ajax({
        type: "POST",
        url: "{{url('delivery/finishRegister')}}",
        data: {data_request: dataJson, _token: "{{csrf_token()}}"},
        dataType: "json",
        cache:false,
        success:
        function(data){
          console.log(data);
          if(data["success"])
          {
            window.location.href = "{{url('commerce/myMenu')}}"; 
          }
          else{
            $("#spanerror").html("error código "+data["error"].code+"");
          }  
        }
      });
      
    });
  });
</script>
@endsection