@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/registerForm.css" rel="stylesheet">
@endsection
@section('title', 'Registro delivery')
@section('maincontent')
<div class="container">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Registro delivery</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 field-label-responsive">
                <label for="name">Usuario</label>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                        <input type="text" name="user_name" class="form-control" id="user_name"
                        placeholder="Nombre de usuario" value="{{ old('user_name') }}" required autofocus>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                 @if ($errors->has('user_name'))
                 <span class="text-danger align-middle">
                   {{ $errors->first('user_name') }}
               </span>   
               @endif
           </div>
       </div>
   </div>
   <div class="row">
    <div class="col-md-3 field-label-responsive">
        <label for="email">E-Mail</label>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                <input type="email" name="email" class="form-control" id="email"
                placeholder="Correo electrónico" value="{{ old('email') }}" required autofocus>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-control-feedback">
            @if ($errors->has('email'))
            <span class="text-danger align-middle">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 field-label-responsive">
        <label for="password">Contraseña</label>
    </div>
    <div class="col-md-6">
        <div class="form-group has-danger">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                <input type="password" name="password" class="form-control" id="password"
                placeholder="Contraseña" required>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-control-feedback">
            @if ($errors->has('password'))
            <span class="text-danger align-middle">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 field-label-responsive">
        <label for="password">Confirmar contraseña</label>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                <div class="input-group-addon" style="width: 2.6rem">
                    <i class="fa fa-repeat"></i>
                </div>
                <input type="password" name="password_confirmation" class="form-control"
                id="password-confirm" placeholder="Confirmar contraseña" required>
            </div>
        </div>
    </div>
     <input type="hidden" name="user_type" class="form-control" id="user_type"
                value="3">
</div>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <button type="submit" class="btn btn-success"><i class="fa fa-user-plus"></i> Registrar delivery</button>
    </div>
</div>
</form>
</div>
@endsection
@section('ajax')

@endsection