@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/shoppingcart.css" rel="stylesheet">
@endsection
@section('title', 'Pedidos')
@section('maincontent')
<div class="container">
  <div class="row">
    <div class="col-lg-12 my-3">
      <div class="pull-right">
        <div class="btn-group">
          <button class="btn btn-info" id="ordenActualLink">
            Actual
          </button>
          <button class="btn btn-info" id="pagadoLink">
            Disponible
          </button>
          <button class="btn btn-info" id="completoLink">
            Completas
          </button>
        </div>
      </div>
    </div>
  </div> 
  <h3>Órdenes de compra</h3>
  <span id="penalized" class="text-danger"></span>
  <div id="ordenActual">
    <span class="text-info" id="spanOrdenActual"></span>
    <div id="map" style="width: 300px; height: 300px"></div>
  </div>
  <div class="count"></div>

  <div class="row" id="mostrarPagado" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Disponibles para tomar</h>
        <table class="table table-striped" id="serviceOrderPagadoClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
              <th scope="col"></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="row" id="mostrarCompleto" style="display: none;">
    <div class="col-12">
      <div class="table-responsive">
        <h>Completas</h>
        <table class="table table-striped" id="serviceOrderCompleteClient">
          <thead>
            <tr>
              <th scope="col">Número de orden</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1542186754" />
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
<script>
  /**
 * Calculates and displays a car route from the Brandenburg Gate in the centre of Berlin
 * to Friedrichstraße Railway Station.
 *
 * A full list of available request parameters can be found in the Routing API documentation.
 * see:  http://developer.here.com/rest-apis/documentation/routing/topics/resource-calculate-route.html
 *
 * @param   {H.service.Platform} platform    A stub class to access HERE services
 */

 function calculateRouteFromAtoB (platform, ub1, ub2) {
  var router = platform.getRoutingService(),
  routeRequestParams = {
    mode: 'fastest;car',
    representation: 'display',
    routeattributes : 'waypoints,summary,shape,legs',
    maneuverattributes: 'direction,action',
    waypoint0: ub1, 
    waypoint1: ub2 
  };


  router.calculateRoute(
    routeRequestParams,
    onSuccess,
    onError
    );
}
/**
 * This function will be called once the Routing REST API provides a response
 * @param  {Object} result          A JSONP object representing the calculated route
 *
 * see: http://developer.here.com/rest-apis/documentation/routing/topics/resource-type-calculate-route.html
 */
 function onSuccess(result) {
  var route = result.response.route[0];
 /*
  * The styling of the route response on the map is entirely under the developer's control.
  * A representitive styling can be found the full JS + HTML code of this example
  * in the functions below:
  */
  addRouteShapeToMap(route);
  addManueversToMap(route);
  // ... etc.
}

/**
 * This function will be called if a communication error occurs during the JSON-P request
 * @param  {Object} error  The error message received.
 */
 function onError(error) {
  alert('Ooops!');
}




/**
 * Boilerplate map initialization code starts below:
 */

// set up containers for the map  + panel
var mapContainer = document.getElementById('map');

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
  'app_id': 'rJiRmujvggTUctAQdVcz',
  'app_code': 'cGTBiKr-8RViLHiufJVeCA',
  useHTTPS: true
});
var pixelRatio = window.devicePixelRatio || 1;
var defaultLayers = platform.createDefaultLayers({
  tileSize: pixelRatio === 1 ? 256 : 512,
  ppi: pixelRatio === 1 ? undefined : 320
});

//Step 2: initialize a map - this map is centered over Berlin
var map = new H.Map(mapContainer,
  defaultLayers.normal.map,{
    center: {lat:52.5160, lng:13.3779},
    zoom: 13,
    pixelRatio: pixelRatio
  });

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);

// Hold a reference to any infobubble opened
var bubble;

/**
 * Opens/Closes a infobubble
 * @param  {H.geo.Point} position     The location on the map.
 * @param  {String} text              The contents of the infobubble.
 */
 function openBubble(position, text){
   if(!bubble){
    bubble =  new H.ui.InfoBubble(
      position,
      // The FO property holds the province name.
      {content: text});
    ui.addBubble(bubble);
  } else {
    bubble.setPosition(position);
    bubble.setContent(text);
    bubble.open();
  }
}


/**
 * Creates a H.map.Polyline from the shape of the route and adds it to the map.
 * @param {Object} route A route as received from the H.service.RoutingService
 */
 function addRouteShapeToMap(route){
  var lineString = new H.geo.LineString(),
  routeShape = route.shape,
  polyline;

  routeShape.forEach(function(point) {
    var parts = point.split(',');
    lineString.pushLatLngAlt(parts[0], parts[1]);
  });

  polyline = new H.map.Polyline(lineString, {
    style: {
      lineWidth: 4,
      strokeColor: 'rgba(0, 128, 255, 0.7)'
    }
  });
  // Add the polyline to the map
  map.addObject(polyline);
  // And zoom to its bounding rectangle
  map.setViewBounds(polyline.getBounds(), true);
}


/**
 * Creates a series of H.map.Marker points from the route and adds them to the map.
 * @param {Object} route  A route as received from the H.service.RoutingService
 */
 function addManueversToMap(route){
  var svgMarkup = '<svg width="18" height="18" ' +
  'xmlns="http://www.w3.org/2000/svg">' +
  '<circle cx="8" cy="8" r="8" ' +
  'fill="#1b468d" stroke="white" stroke-width="1"  />' +
  '</svg>',
  dotIcon = new H.map.Icon(svgMarkup, {anchor: {x:8, y:8}}),
  group = new  H.map.Group(),
  i,
  j;

  // Add a marker for each maneuver
  for (i = 0;  i < route.leg.length; i += 1) {
    for (j = 0;  j < route.leg[i].maneuver.length; j += 1) {
      // Get the next maneuver.
      maneuver = route.leg[i].maneuver[j];
      // Add a marker to the maneuvers group
      var marker =  new H.map.Marker({
        lat: maneuver.position.latitude,
        lng: maneuver.position.longitude} ,
        {icon: dotIcon});
      marker.instruction = maneuver.instruction;
      group.addObject(marker);
    }
  }

  group.addEventListener('tap', function (evt) {
    map.setCenter(evt.target.getPosition());
    openBubble(
     evt.target.getPosition(), evt.target.instruction);
  }, false);

  // Add the maneuvers group to the map
  map.addObject(group);
}

function sendParametersCalculateRouteFromAtoB(ub1, ub2){
  calculateRouteFromAtoB (platform, ub1, ub2);
}

</script>
<script>
  $(function() {
// botones: pendientePagoLink - pagadoLink - enTransitoLink - completoLink
// mostrarPendiente - mostrarPagado - mostrarEnTransito - mostrarCompeto
$('#ordenActualLink').click(function(e) {
  $("#ordenActual").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#pagadoLink').click(function(e) {
  $("#mostrarPagado").delay(100).fadeIn(100);
  $("#ordenActual").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $('#ordenActualLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#enTransitoLink').click(function(e) {
  $("#mostrarEnTransito").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#ordenActual").fadeOut(100);
  $("#mostrarCompleto").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#ordenActualLink').removeClass('active');
  $('#completoLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

$('#completoLink').click(function(e) {
  $("#mostrarCompleto").delay(100).fadeIn(100);
  $("#mostrarPagado").fadeOut(100);
  $("#ordenActual").fadeOut(100);
  $("#mostrarEnTransito").fadeOut(100);
  $('#pagadoLink').removeClass('active');
  $('#ordenActualLink').removeClass('active');
  $('#enTransitoLink').removeClass('active');
  $(this).addClass('active');
  e.preventDefault();
});

});
</script>
@endsection
@section('ajax')
<script src="{{url('')}}/js/datatables.min.js"></script>
<script>

  function startCounter(){
//Counter
var counter=0;
if(typeof(sessionStorage.getItem('counts'))!='object')
{
 counter=parseInt(sessionStorage.getItem('counts'));
}
var timer = setInterval(function () {
  if(counter < 50){
    console.log(counter);
    ++counter;
    sessionStorage.setItem('counts',counter);
  } else {
    penalizeDelivery();
    clearInterval(timer);
  }
}, 1000);

}


function penalizeDelivery(){
  var dataFinishOrder =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
    }
  };

  dataFinishOrderJson =  JSON.stringify(dataFinishOrder);

  $.ajax({
    type: "POST",
    url: "{{url('delivery/penalizeDelivery')}}",
    data: {data_request: dataFinishOrderJson, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
      console.log(data);
      $("#penalized").html("Ha sido penalizado por permanecer inactivo durante 15 minutos.");
   }
 });

}


function finishOrder(id){
  var dataFinishOrder =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_service_order": id
    }
  };

  dataFinishOrderJson =  JSON.stringify(dataFinishOrder);

  $.ajax({
    type: "POST",
    url: "{{url('delivery/markAsShipped')}}",
    data: {data_request: dataFinishOrderJson, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
      console.log(data);
   location.reload();         
   }
 });

}

function takeOrder(id){
  var dataAssign =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_service_order": id
    }
  };

  dataJsonAssign =  JSON.stringify(dataAssign);
  console.log(dataJsonAssign);

  $.ajax({
    type: "POST",
    url: "{{url('delivery/assignOrder')}}",
    data: {data_request: dataJsonAssign, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
      sessionStorage.clear();
      location.reload();         
    }
  });

}

function markAsPickedUp(id){
  var dataAssign =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_service_order": id
    }
  };

  dataJsonAssign =  JSON.stringify(dataAssign);
  console.log(dataJsonAssign);

  $.ajax({
    type: "POST",
    url: "{{url('delivery/markAsPickedUp')}}",
    data: {data_request: dataJsonAssign, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
     location.reload();         
    }
  });

}

$(document).ready(function(){

  var dataPagado =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "2"
    }
  };

  dataPagadoJson =  JSON.stringify(dataPagado);

  $('#serviceOrderPagadoClient').DataTable({
    "lengthChange": false,
    "bFilter": false,
    "info": false,
    "ajax":{
      "type": "POST",
      "url": "{{url('/delivery/getServiceOrder')}}",
      "data": {data_request: dataPagadoJson, _token: "{{csrf_token()}}"},
      "dataType": "json",
      "dataSrc":""
    },
    "columns":[
    {"data":"id"},
    {"data":"price"},
    {"data":"date.date"},
    {"data":"null", render: function(data,type,row,meta){
      if(data==null){
        return '<a class="btn btn-primary text-white" onClick="takeOrder('+row.id+')">Tomar</a>';
      }
    }
  }
  ]
});

  var dataTransito =
  {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_state": "3"
    }
  };

  dataTransitoJson =  JSON.stringify(dataTransito);

  $.ajax({
    type: "POST",
    url: "{{url('delivery/getServiceOrderWithStateThree')}}",
    data: {data_request: dataTransitoJson, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
      if(data==""){
        var data =
        {
          "auth_parameters":
          {
            "id_user": {{Auth::user()->id}},
            "id_user_type" : {{Auth::user()->user_type}},
            "auth_token" : "{{Auth::user()->auth_token}}",
          },
          "parameters":
          {
            "id_state": "4"
          }
        };
        dataJson =  JSON.stringify(data);

        $.ajax({
          type: "POST",
          url: "{{url('delivery/getServiceOrderWithStateThree')}}",
          data: {data_request: dataJson, _token: "{{csrf_token()}}"},
          dataType: "json",
          cache:false,
          success:
          function(data){
            if(data==""){
              $("#mostrarPagado").delay(100).fadeIn(100);
              $("#ordenActual").delay(100).fadeOut(100);          
              $("#ordenActualLink").attr("disabled", true);
              startCounter();
            }else{
             $.each(data, function(i, item) {
              $("#spanOrdenActual").html("Actualmente tiene un pedido activo. ID del pedido: " + data[i].id + "");
              $("#ordenActual").append('<a class="btn btn-primary text-white" onClick="finishOrder('+data[i].id+')">Terminar</a>');
              $("#pagadoLink").attr("disabled", true);
              var idClient = data[i].id_client;
              var idCommerce = data[i].id_commerce;
              obtainGeoPosition(idClient, idCommerce);
            });     
           }
         }
       });
      }else{
        console.log(data);
        $.each(data, function(i, item) {
          $("#spanOrdenActual").html("Actualmente tiene un pedido activo pendiente de recoger. ID del pedido: " + data[i].id + "");
          $("#ordenActual").append('<a class="btn btn-primary text-white" onClick="markAsPickedUp('+data[i].id+')">Marcar como recogido</a>');
          $("#pagadoLink").attr("disabled", true);
          var idCommerce = data[i].id_commerce;
          obtainGeoPositionForCommerceOnly(idCommerce);
        });   
      }   
    }
  });


  function obtainGeoPositionForCommerceOnly(idCommerce){
   var data =
   {
    "auth_parameters":
    {
      "id_user": {{Auth::user()->id}},
      "id_user_type" : {{Auth::user()->user_type}},
      "auth_token" : "{{Auth::user()->auth_token}}",
    },
    "parameters":
    {
      "id_commerce": idCommerce
    }
  };
  dataJson =  JSON.stringify(data);

  $.ajax({
    type: "POST",
    url: "{{url('/commerce/getGeoPosition')}}",
    data: {data_request: dataJson, _token: "{{csrf_token()}}"},
    dataType: "json",
    cache:false,
    success:
    function(data){
      var ub1 =""+data.lat+","+data.long+"";
      var ub2 =""+data.lat+","+data.long+""; 
      sendParametersCalculateRouteFromAtoB(ub1, ub2);
    }
  });

}

function obtainGeoPosition(idClient, idCommerce){
 var data =
 {
  "auth_parameters":
  {
    "id_user": {{Auth::user()->id}},
    "id_user_type" : {{Auth::user()->user_type}},
    "auth_token" : "{{Auth::user()->auth_token}}",
  },
  "parameters":
  {
    "id_client": idClient
  }
};
dataJson =  JSON.stringify(data);

var dataCommerce =
{
  "auth_parameters":
  {
    "id_user": {{Auth::user()->id}},
    "id_user_type" : {{Auth::user()->user_type}},
    "auth_token" : "{{Auth::user()->auth_token}}",
  },
  "parameters":
  {
    "id_commerce": idCommerce
  }
};
dataCommerceJson =  JSON.stringify(dataCommerce);

$.ajax({
  type: "POST",
  url: "{{url('/client/getGeoPosition')}}",
  data: {data_request: dataJson, _token: "{{csrf_token()}}"},
  dataType: "json",
  cache:false,
  success:
  function(data){
    var ub1 =""+data.lat+","+data.long+"";
    $.ajax({
      type: "POST",
      url: "{{url('/commerce/getGeoPosition')}}",
      data: {data_request: dataCommerceJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        var ub2 =""+data.lat+","+data.long+"";
        sendParametersCalculateRouteFromAtoB(ub1, ub2);
      }
    });
  }
});

}

var dataCompleto =
{
  "auth_parameters":
  {
    "id_user": {{Auth::user()->id}},
    "id_user_type" : {{Auth::user()->user_type}},
    "auth_token" : "{{Auth::user()->auth_token}}",
  },
  "parameters":
  {
    "id_state": "5"
  }
};

dataCompletoJson =  JSON.stringify(dataCompleto);

$('#serviceOrderCompleteClient').DataTable({
  "lengthChange": false,
  "bFilter": false,
  "info": false,
  "ajax":{
    "type": "POST",
    "url": "{{url('/delivery/getServiceOrderWithStateThree')}}",
    "data": {data_request: dataCompletoJson, _token: "{{csrf_token()}}"},
    "dataType": "json",
    "dataSrc":""
  },
  "columns":[
  {"data":"id"},
  {"data":"price"},
  {"data":"date.date"},
  ]
});


});
</script>
@endsection