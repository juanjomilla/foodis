@extends('layouts.mainTemplate')

@section('title', 'Inicio')

@section('maincontent')
  <div class="masthead">
    <!-- for Responsive Viewport (Insert Carousel or other item for "cover" effect)  -->
    <!-- Carousel container -->
    <div id="suds-carousel" class="carousel slide fade-carousel" data-ride="carousel">
      <!-- Content -->
      <div class="carousel-inner carousel-zoom" role="listbox">
        <!-- Slide 1 -->
        <div class="carousel-item active">
          <div class="slide-1"></div>
          <div class="carousel-caption">
            <hgroup>
              <h1>Hacé tu pedido</h1>
              <div class="input-group input-group-lg">
                <select id="city" name="city">
                  <option value="0">Ciudad</option>
                  <option value="1">Buenos aires</option>
                  <!-- php para el option -->
                </select>
                <input type="text" class="form-control" id="address" name="address" placeholder="Dirección">
                <span class="input-group-btn">
                  <button class="btn btn-danger btn-block" type="button">
                    <i class="fa fa-search " aria-hidden="true"></i>
                  </button>
                </span>
              </div>
            </hgroup>
          </div>
        </div>
      </div>
    </div>
  </div>
  <main class="main pt-4" role="main">
    <div class="intro">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <a class="intro-block" href="" style="background-image:url('img/linguine.jpg')">
              <div class="intro-block-inner">
                <h2>Restaurantes</h2>
              </div>
            </a>
          </div>
          <div class="col-md-4">
            <a class="intro-block" href="" style="background-image:url('img/pie.jpg')">
              <div class="intro-block-inner">
                <h2>Cafetería</h2>
              </div>
            </a>
          </div>
          <div class="col-md-4">
            <a class="intro-block" href="" style="background-image:url('img/hamburguer.jpg')">
              <div class="intro-block-inner">
                <h2>Comida rápida</h2>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>
  </main>
@stop