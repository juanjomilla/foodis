@extends('layouts.mainTemplate')
@section('css')
<link href="{{url('')}}/css/showMenues.css" rel="stylesheet">
@endsection
@section('title', 'Cuenta deshabilitada')
@section('maincontent')
<div class="container">
    <div class="row">
      <h3 class="text-danger">Su cuenta se encuentra inactiva. Si acaba de registrarse espere a ser habilitado, si cree que es un error contacte con el administrador.</h3>        
    </div> 

</div>
@endsection
