<html>
<head>
  <link href="{{url('')}}/css/index.css" rel="stylesheet">
  <link href="{{url('')}}/css/bootstrap-4/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  @yield('css')
  <title> Foodis | @yield('title')</title>

</head>
<body>
  <header role="banner">
    <nav class="navbar navbar-expand-md navbar-light bg-white absolute-top">
      <div class="container">

        <button class="navbar-toggler order-2 order-md-1" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand mx-auto order-1" href="{{url('/home')}}">Foodis</a>
        <div class="collapse navbar-collapse order-4 order-md-4" id="navbar">
          <ul class="navbar-nav ml-auto">
            <form class="form-inline" role="search">
              <input class="search js-search form-control form-control-rounded mr-sm-2" title="" placeholder="Buscar"  type="text">
            </form>
            <li class="nav-item">
              @if(Auth::check())
              <div class="dropdown show">
                <a class="dropdown-toggle text-secondary" href="#" id="dropdownUser" data-toggle="dropdown" >
                  {{Auth::user()->user_name}}
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownUser" id="dropDownMenu">
                </div>
              </div>
              @else
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/client/register') }}" id="signup">Registrate</a>
              </li>
              <a class="nav-link" href="login" id="{{ url('/login') }}">Iniciar sesión</a>
              @endif
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  @yield('maincontent')

  <footer class="site-footer bg-darkest" role="contentinfo">
    <div class="container">

      <ul class="nav justify-content-center">
        <li class="nav-item">
          <a class="nav-link" href="nosotros">Nosotros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contacto">Contacto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/commerce/register')}}">Registrate como comercio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/delivery/register')}}">Trabajá como delivery</a>
        </li>
      </ul>
      <div class="copy">
        © Foodis 2018<br>
      </div>
    </div>
  </footer>
</body>
<script src="{{url('')}}/js/jquery/jquery-3.3.1.min.js"></script>
<script src="{{url('')}}/js/bootstrap/bootstrap.min.js"></script>
<script src="{{url('')}}/js/datatables.min.js"></script>

<script>
  $(document).ready(function(){
    @if(Auth::check())
          var data =
      {
        parameters:
        {          
          id_user_type: "{{Auth::user()->user_type}}"
        }
      };
    let dataJson = JSON.stringify(data);

    $.ajax({
      type: "POST",
      url: "{{url('/getHeaderLinks')}}" ,
      data: {data_request: dataJson, _token: "{{csrf_token()}}"},
      dataType: "json",
      cache:false,
      success:
      function(data){
        console.log(data);
        $.each(data, function(i, item) {
          $("#dropDownMenu").append ('<a class="dropdown-item" href="'+ data[i].link+'">'+data[i].text+'</a>');

        });
         $("#dropDownMenu").append('<a class="dropdown-item" href="{{url("logout")}}">Cerrá sesión</a>');

      }
    });
    @endif
  });
</script>
@yield('js')
@yield('ajax')

</html>
