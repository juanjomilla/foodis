Bienvenido {{ $content->userName }},
Este es un email para confirmar que te has registrado correctamente como {{ $content->user_type }}
{{ $content->optional_message }}

Gracias por confiar en nosotros,

{{ $content->sender }}