<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 27/11/2018
 * Time: 20:51
 */
?>

Bienvenido <i>{{ $content->userName }}</i>,
<p>Este es un email para confirmar que te has registrado correctamente como {{ $content->user_type }}</p>
<p>{{ $content->optional_message }}</p>

Gracias por confiar en nosotros,
<br/>
<i>{{ $content->sender }}</i>