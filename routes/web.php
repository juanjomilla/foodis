<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/logout', function () {
    \Auth::logout();
    return redirect('/home');
});

Route::post('/getHeaderLinks', 'HomeController@getHeaderLinks');


// Rutas de registro segunda parte//////////////////////
Route::get('/commerce/finishRegister',  function() {
    return view('commerce.finishRegister');
})->middleware('auth');
Route::post('/commerce/finishRegister', 'Commerce\CommerceController@finishRegister');

Route::get('/client/finishRegister',  function() {
    return view('client.finishRegister');
})->middleware('auth');
Route::post('/client/finishRegister', 'Client\ClientController@finishRegister');

Route::get('/delivery/finishRegister',  function() {
    return view('delivery.finishRegister');
})->middleware('auth');
Route::post('/delivery/finishRegister', 'Delivery\DeliveryController@finishRegister');

Route::group(['middleware' => 'finishRegister'], function() {
    ///
    // Rutas de registro primera parte/////////////////////


    Auth::routes();

    Route::get('/commerce/register',  function() {
        return view('commerce.registerCommerce');
    });

    Route::get('/client/register',  function() {
        return view('client.registerClient');
    });

    Route::get('/delivery/register',  function() {
        return view('delivery.registerDelivery');
    });
    ////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
// Rutas relacionadas a menúes /////////////////////////

    Route::get('/commerce/myMenu',  function () {
        return view('commerce.showMenues');
    });
    Route::post('/commerce/showMenues', 'Commerce\CommerceController@getAllMenuFromCommerce');

    Route::get('commerce/addMenu',  function () {
        return view('commerce.addMenu');
    });
    Route::post('/commerce/saveMenu', 'Commerce\CommerceController@saveMenu');

    Route::get('/client/menu/{idCommerce}',  function ($idCommerce) {
        return view('client.showMenues')
        ->with('idCommerce', $idCommerce);
    });

    Route::post('/menuFromCommerce', 'Client\ClientController@getMenuFromCommerce');

////////////////////////////////////////////////////////
// Rutas relacionadas a ver comercios /////////////////////
    Route::get('/client/commerces',  function () {
        return view('client.showCommerces');
    });
    Route::post('/client/showCommerce', 'Client\ClientController@getAllCommercesFromUser');
////////////////////////////////////////////////////////
// Rutas relacionadas al carrito de compras //////////////////
    Route::get('/client/cart',  function () {
        return view('client.showShoppingCart');
    });
    Route::post('/client/showCart', 'Client\ClientController@getCartFromUser');

    Route::post('/client/addToCart', 'Client\ClientController@addToCart');

    Route::post('/client/emptyCart/', 'Client\ClientController@emptyCart');

    Route::post('/client/getIdCommerceFromUserCart', 'Client\ClientController@getIdCommerceFromUserCart');
    Route::post('/client/confirmCart', 'Client\ClientController@confirmCart');

    Route::post('/client/updateItemQty', 'Client\ClientController@updateItemQty');

////////////////////////////////////////////////////////
//Rutas relacionadas al serviceOrder///////////////////
    Route::get('/client/serviceOrder',  function () {
        return view('client.showServiceOrder');
    });
    Route::post('/client/getServiceOrder', 'Client\ClientController@getServiceOrder');

    Route::get('/delivery/serviceOrder',  function () {
        return view('delivery.showServiceOrder');
    });
    Route::post('/delivery/getServiceOrder', 'Delivery\DeliveryController@getServiceOrder');
    Route::get('/commerce/serviceOrder',  function () {
        return view('commerce.showServiceOrder');
    });
    Route::post('/commerce/getServiceOrder', 'Commerce\CommerceController@getServiceOrder');

    Route::post('/delivery/assignOrder', 'Delivery\DeliveryController@assignOrder');

    Route::post('/delivery/markAsShipped', 'Delivery\DeliveryController@markAsShipped');
    
    Route::post('/delivery/markAsPickedUp', 'Delivery\DeliveryController@markAsPickedUp');

    Route::post('/delivery/getServiceOrderWithStateThree', 'Delivery\DeliveryController@getServiceOrderWithStateThree');

    Route::post('/delivery/penalizeDelivery', 'Delivery\DeliveryController@penalizeDelivery');

});

////////////////////////////////////////////////////////
// Rutas relacionadas al panel de administración //////
Route::get('/client/admin',  function () {
    return view('client.adminPanel');
});

////////////////////////////////////////////////////////
// Rutas relacionadas al ADMIN //////
Route::get('/admin/commercesList',  function () {
    return view('admin.commercesList');
});

Route::get('/admin/deliveriesList',  function () {
    return view('admin.deliveresList');
});

Route::get('/admin/settlement',  function () {
    return view('admin.serviceOrder');
});
Route::post('/admin/getCompletedServiceOrderWithoutClearing', 
 'Admin\AdminController@getCompletedServiceOrderWithoutClearing');

Route::post('/admin/getCompletedServiceOrderWithClearing', 
 'Admin\AdminController@getCompletedServiceOrderWithClearing');

Route::post('/admin/makeClearing', 
 'Admin\AdminController@makeClearing');

Route::post('/admin/getCommercesDisabled', 
 'Admin\AdminController@getCommercesDisabled');

Route::post('/admin/getCommercesEnabled', 
 'Admin\AdminController@getCommercesEnabled');

Route::post('/admin/enableCommerce', 
 'Admin\AdminController@enableCommerce');

Route::post('/admin/disableCommerce', 
 'Admin\AdminController@disableCommerce');

Route::post('/admin/getDeliveriesDisabled', 
 'Admin\AdminController@getDeliveriesDisabled');

Route::post('/admin/getDeliveriesEnabled', 
 'Admin\AdminController@getDeliveriesEnabled');

Route::post('/admin/enableDelivery', 
 'Admin\AdminController@enableDelivery');

Route::post('/admin/disableDelivery', 
 'Admin\AdminController@enableDelivery');
////////////////////////////////////////////////////////
// Rutas relacionadas a obtener geolocalización //////

Route::post('/client/getGeoPosition', 'Client\ClientController@getGeoPosition');

Route::post('/commerce/getGeoPosition', 'Commerce\CommerceController@getGeoPosition');

////////////////////////////////////////////////////////
// Rutas relacionadas a email /////////////////////

Route::get('mail/send', 'Mail\MailController@send');